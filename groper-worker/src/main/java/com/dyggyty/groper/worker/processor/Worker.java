package com.dyggyty.groper.worker.processor;

import com.dyggyty.groper.model.TestCaseStep;
import com.dyggyty.groper.worker.GroperScriptableObject;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.mozilla.javascript.ClassShutter;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Worker implements ApplicationContextAware {

    private ApplicationContext applicationContext;
    private Context cx;
    private Scriptable scope;

    @PostConstruct
    public void init() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        cx = Context.enter();
        cx.getWrapFactory().setJavaPrimitiveWrap(false);
        cx.setClassShutter(new ClassShutter() {
            public boolean visibleToScripts(String className) {
                return className.startsWith("com.dyggyty.groper.worker.jsmodel.") || className.startsWith("java.lang.");
            }
        });

        scope = cx.initStandardObjects();

        // Use the Counter class to define a Counter constructor
        // and prototype in JavaScript.
        Map<String, GroperScriptableObject> processorsMap =
                BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, GroperScriptableObject.class);

        for (GroperScriptableObject scriptableObject : processorsMap.values()) {
            ScriptableObject.defineClass(scope, scriptableObject.getClass());

            Scriptable scriptableObjectInst = cx.newObject(scope, scriptableObject.getClassName());
            scope.put(scriptableObject.getContextName(), scope, scriptableObjectInst);
        }
    }

    public String runScript(List<TestCaseStep> testCaseSteps) throws ReflectiveOperationException {

        StringBuilder jsResult = new StringBuilder();

        try {
            for (TestCaseStep testCaseStep : testCaseSteps) {
                String stepName = testCaseStep.getCaseStepName();
                if (jsResult.length() > 0) {
                    jsResult.append("\n");
                }

                jsResult.append(stepName).append(": ");

                Object result = cx.evaluateString(scope, testCaseStep.getScript(), stepName, 0, null);
                jsResult.append(Context.toString(result));
            }
        } catch (RuntimeException we) {
            jsResult.append(we.getLocalizedMessage());
        } finally {
            Context.exit();
        }

        return jsResult.toString();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
