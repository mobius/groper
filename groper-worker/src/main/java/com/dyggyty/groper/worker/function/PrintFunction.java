package com.dyggyty.groper.worker.function;

import com.dyggyty.groper.worker.GroperScriptableObject;
import org.mozilla.javascript.BaseFunction;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author mobius
 */
@Component
public class PrintFunction extends BaseFunction implements GroperScriptableObject {

    @Override
    public String getClassName() {
        return getClass().getSimpleName();
    }

    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        String printedValue = getPrintedValue(args);
        System.out.print(printedValue);
        return printedValue;
    }

    protected String getPrintedValue(Object[] args) {
        Object objectToPrint;
        if (args[0] instanceof NativeJavaObject) {
            objectToPrint = ((NativeJavaObject) args[0]).unwrap();
        } else {
            objectToPrint = args[0];
        }

        return String.format(getObjectString(objectToPrint), Arrays.copyOfRange(args, 1, args.length));
    }

    private String getObjectString(Object objToString) {
        if (objToString == null) {
            return "";
        }

        if (objToString instanceof Object[]) {
            StringBuilder result = new StringBuilder();
            for (Object objToPrint : (Object[]) objToString) {
                if (result.length() > 0) {
                    result.append(", ");
                }

                result.append(getObjectString(objToPrint));
            }

            return "{" + result.toString() + "}";
        }

        return objToString.toString();
    }

    public int getArity() {
        return 1;
    }

    @Override
    public String getContextName() {
        return "print";
    }
}
