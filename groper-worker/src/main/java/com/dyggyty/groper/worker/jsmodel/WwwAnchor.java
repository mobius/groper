package com.dyggyty.groper.worker.jsmodel;

/**
 * @author mobius
 */
public class WwwAnchor extends WwwElement {
    private String href;
    private String text;

    public WwwAnchor() {
    }

    public WwwAnchor(String href, String text) {
        this.href = href;
        this.text = text;
    }

    public WwwAnchor(String id, String name, String href, String text) {
        super(id, name);
        this.href = href;
        this.text = text;
    }

    public String jsFunction_getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String jsFunction_getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

	public String getHref() {
		return href;
	}

	public String getText() {
		return text;
	}

	@Override
    public String toString() {
        return "WwwAnchor{" +
                "href='" + href + '\'' +
                ", text='" + text + '\'' +
                "} " + super.toString();
    }
}
