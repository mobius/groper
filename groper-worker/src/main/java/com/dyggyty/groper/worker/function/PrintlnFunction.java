package com.dyggyty.groper.worker.function;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
public class PrintlnFunction extends PrintFunction {

    @Override
    public String getClassName() {
        return getClass().getSimpleName();
    }

    @Override
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        String printedValue = getPrintedValue(args);
        System.out.println(printedValue);
        return printedValue;
    }

    @Override
    public String getContextName() {
        return "println";
    }
}
