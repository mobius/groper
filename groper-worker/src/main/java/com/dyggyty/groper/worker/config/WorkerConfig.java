package com.dyggyty.groper.worker.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author mobius
 */
@Configuration
@ComponentScan(basePackages = {"com.dyggyty.groper.worker.function", "com.dyggyty.groper.worker.processor"},
        excludeFilters = {@ComponentScan.Filter(Configuration.class)})
public class WorkerConfig {
}
