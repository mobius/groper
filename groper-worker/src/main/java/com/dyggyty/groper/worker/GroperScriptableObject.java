package com.dyggyty.groper.worker;

import org.mozilla.javascript.Scriptable;

/**
 * @author mobius
 */
public interface GroperScriptableObject extends Scriptable {

    public String getContextName();
}
