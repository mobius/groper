package com.dyggyty.groper.worker.jsmodel;

/**
 * @author mobius
 */
public abstract class WwwElement {
    private String id;
    private String name;

    protected WwwElement() {
    }

    protected WwwElement(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String jsFunction_getId() {
        return id;
    }

    public String jsFunction_getName() {
        return name;
    }

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WwwElement{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
