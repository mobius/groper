package com.dyggyty.groper.worker.processor;

import com.dyggyty.groper.worker.GroperScriptableObject;
import com.dyggyty.groper.worker.jsmodel.WwwAnchor;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.mozilla.javascript.ScriptableObject;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;

/**
 * @author mobius
 */
@Component
public class WwwProcessor extends ScriptableObject implements GroperScriptableObject {

    private final WebClient webClient;
    private Page page;

    private static final String FIELD_TYPE_NAME = "name";
    private static final String FIELD_TYPE_ID = "id";
    private static final String FIELD_TYPE_HREF = "href";
    private static final String FIELD_TYPE_TEXT = "text";
    private static final long BACKGROUND_JS_TIMEOUT = 10000; //todo made this configurable

    public WwwProcessor() {
        webClient = new WebClient(BrowserVersion.CHROME);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());

        CookieManager cookieManager = webClient.getCookieManager();
        cookieManager.clearCookies();
        cookieManager.setCookiesEnabled(true);

        WebClientOptions webClientOptions = webClient.getOptions();
        webClientOptions.setActiveXNative(false);
        webClientOptions.setCssEnabled(true);
        webClientOptions.setJavaScriptEnabled(true);
        webClientOptions.setUseInsecureSSL(true);
        webClientOptions.setRedirectEnabled(true);
        webClientOptions.setTimeout(30000);
    }

    @Override
    public String getClassName() {
        return getClass().getSimpleName();
    }

    public int jsFunction_open(String url) throws IOException {
        page = webClient.getPage(url);
        webClient.waitForBackgroundJavaScript(BACKGROUND_JS_TIMEOUT);
        return page.getWebResponse().getStatusCode();
    }

    public void jsFunction_setValue(String field, String value) {

        HtmlElement htmlElement = getHtmlElement(field);

        if (htmlElement instanceof HtmlCheckBoxInput) {
            ((HtmlCheckBoxInput) htmlElement).setChecked(Boolean.valueOf(value));
        } else if (htmlElement instanceof HtmlInput) {
            ((HtmlInput) htmlElement).setValueAttribute(value == null ? "" : value);
        } else {
            throw new IllegalAccessError("HTML element in not type of HtmlInput: " + htmlElement.getClass().getSimpleName());
        }
    }

    public void jsFunction_clickButton(String field) throws IOException {
        HtmlElement htmlElement = getHtmlElement(field);

        page = htmlElement.click();
        webClient.waitForBackgroundJavaScript(BACKGROUND_JS_TIMEOUT);

        if (!(htmlElement instanceof HtmlButton)) {
            //todo throw warning "HTML element in not type of HtmlButton: " + htmlElement.getClass().getSimpleName();
        }
    }

    public void jsFunction_submitForm(String formName) throws IOException {
        HtmlPage htmlPage = getHtmlPage();

        // create a submit button - it doesn't work with 'input'
        HtmlElement button = (HtmlButton) htmlPage.createElement("button");
        button.setAttribute("type", "submit");

        HtmlElement form = getHtmlForm(formName);
        form.appendChild(button);

        page = button.click();
        webClient.waitForBackgroundJavaScript(10000);
    }

    public String jsFunction_getValue(String field) {
        HtmlElement htmlElement = getHtmlElement(field);

        if (htmlElement instanceof HtmlCheckBoxInput) {
            return Boolean.toString(((HtmlCheckBoxInput) htmlElement).isChecked());
        } else if (htmlElement instanceof HtmlInput) {
            return ((HtmlInput) htmlElement).getValueAttribute();
        }

        return htmlElement.getTextContent();
    }

    public WwwAnchor jsFunction_getWwwAnchor(String field) {
        return createWwwAnchor(getHtmlAnchor(field));
    }

    public void jsFunction_clickWwwAnchor(String field) throws IOException {
        page = getHtmlAnchor(field).click();
        webClient.waitForBackgroundJavaScript(BACKGROUND_JS_TIMEOUT);
    }

    public List<WwwAnchor> jsFunction_getWwwAnchors(String field) {
        List<WwwAnchor> wwwAnchors = new ArrayList<>();

        for (HtmlAnchor htmlAnchor : getHtmlPage().getAnchors()) {
            wwwAnchors.add(createWwwAnchor(htmlAnchor));
        }

        return wwwAnchors;
    }

    public String[] jsFunction_getValuesByXpath(String xpath) {

        if (page == null) {
            throw new IllegalStateException("HTML form was not loaded.");
        }

        if (!(page instanceof DomNode)) {
            throw new IllegalStateException("The page does not contain XML content.");
        }

        if (xpath == null || xpath.trim().isEmpty()) {
            throw new IllegalArgumentException("XPath could not be empty");
        }

        List elements = ((DomNode) page).getByXPath(xpath);

        List<String> results = new ArrayList<>();
        for (Object element : elements) {
            if (element instanceof Double || element instanceof Boolean || element instanceof String) {
                results.add(element.toString());
            } else if (element instanceof HtmlCheckBoxInput) {
                results.add(Boolean.toString(((HtmlCheckBoxInput) element).isChecked()));
            } else if (element instanceof HtmlInput) {
                results.add(((HtmlInput) element).getValueAttribute());
            } else if (element instanceof Node) {
                results.add(((Node) element).getTextContent());
            } else {
                throw new RuntimeException("Unprocessed " + element.getClass().getName());
            }
        }

        return results.toArray(new String[results.size()]);
    }

    private HtmlPage getHtmlPage() {

        if (page == null) {
            throw new IllegalStateException("HTML form was not loaded.");
        }

        if (!page.isHtmlPage()) {
            throw new IllegalStateException("The page does not contain HTML content.");
        }

        return (HtmlPage) page;
    }

    private HtmlForm getHtmlForm(String formName) {

        if (formName == null || formName.trim().isEmpty()) {
            throw new IllegalArgumentException("Form name could not be empty");
        }

        HtmlPage htmlPage = getHtmlPage();

        HtmlForm htmlElement = null;
        String[] fieldTypeDetails = formName.split("=");
        if (fieldTypeDetails.length > 1) {
            String fieldName = fieldTypeDetails[1];
            String fieldType = fieldTypeDetails[0].toLowerCase();
            if (fieldName == null || fieldName.trim().isEmpty()) {
                throw new IllegalArgumentException("Field name could not be empty");
            }

            if (FIELD_TYPE_ID.equals(fieldType)) {
                for (HtmlForm htmlForm : htmlPage.getForms()) {
                    if (fieldName.equalsIgnoreCase(htmlForm.getId())) {
                        htmlElement = htmlForm;
                        break;
                    }
                }
            } else if (FIELD_TYPE_NAME.equals(fieldType)) {
                htmlElement = htmlPage.getFormByName(fieldName);
            }
        } else {
            for (HtmlForm htmlForm : htmlPage.getForms()) {
                if (formName.equalsIgnoreCase(htmlForm.getId())) {
                    htmlElement = htmlForm;
                    break;
                }
            }

            if (htmlElement == null) {
                htmlElement = htmlPage.getFormByName(formName);
            }
        }

        if (htmlElement == null) {
            throw new NullPointerException("Can't find form " + formName);
        }

        return htmlElement;
    }

    private HtmlAnchor getHtmlAnchor(String field) {
        HtmlPage htmlPage = getHtmlPage();

        HtmlAnchor htmlAnchor = null;
        String[] fieldTypeDetails = field.split("=");
        if (fieldTypeDetails.length > 1) {
            String fieldName = fieldTypeDetails[1];
            String fieldType = fieldTypeDetails[0].toLowerCase();
            if (fieldName == null || fieldName.trim().isEmpty()) {
                throw new IllegalArgumentException("Field name could not be empty");
            }

            switch (fieldType) {
                case FIELD_TYPE_ID: {
                    htmlAnchor = htmlPage.getHtmlElementById(fieldName);
                    break;
                }
                case FIELD_TYPE_NAME: {
                    htmlAnchor = htmlPage.getAnchorByName(fieldName);
                    break;
                }
                case FIELD_TYPE_HREF: {
                    htmlAnchor = htmlPage.getAnchorByHref(fieldName);
                    break;
                }
                case FIELD_TYPE_TEXT: {
                    htmlAnchor = htmlPage.getAnchorByText(fieldName);
                    break;
                }
            }
        } else {
            try {
                htmlAnchor = htmlPage.getHtmlElementById(field);
            } catch (ElementNotFoundException enfe) {
                try {
                    htmlAnchor = htmlPage.getAnchorByName(field);
                } catch (ElementNotFoundException enfe1) {
                    try {
                        htmlAnchor = htmlPage.getAnchorByHref(field);
                    } catch (ElementNotFoundException enfe2) {
                        htmlAnchor = htmlPage.getAnchorByText(field);
                    }
                }
            }
        }

        return htmlAnchor;
    }


    private HtmlElement getHtmlElement(String field) {
        HtmlPage htmlPage = getHtmlPage();
        return getHtmlElement(htmlPage, field);
    }

    private HtmlElement getHtmlElement(HtmlPage htmlPage, String field) {

        if (field == null || field.trim().isEmpty()) {
            throw new IllegalArgumentException("Field could not be empty");
        }

        HtmlElement htmlElement = null;
        String[] fieldType = field.split("=");
        if (fieldType.length > 1) {
            String fieldName = fieldType[1];
            if (fieldName == null || fieldName.trim().isEmpty()) {
                throw new IllegalArgumentException("Field name could not be empty");
            }

            if (FIELD_TYPE_ID.equals(fieldType[0])) {
                htmlElement = htmlPage.getHtmlElementById(fieldName);
            } else if (FIELD_TYPE_NAME.equals(fieldType[0])) {
                htmlElement = htmlPage.getElementByName(fieldName);
            }
        } else {
            try {
                htmlElement = htmlPage.getHtmlElementById(field);
            } catch (ElementNotFoundException enfe) {
                htmlElement = htmlPage.getElementByName(field);
            }
        }

        return htmlElement;
    }

    private static WwwAnchor createWwwAnchor(HtmlAnchor htmlAnchor) {
        return new WwwAnchor(htmlAnchor.getId(), htmlAnchor.getNameAttribute(),
                htmlAnchor.getHrefAttribute(), htmlAnchor.getTextContent());
    }

    @Override
    public String getContextName() {
        return "www";
    }
}
