package com.dyggyty.groper.facade;

import com.dyggyty.groper.model.Project;
import java.util.List;

/**
 * @author mobius
 */
public interface ProjectFacade {

    public List<Project> getUserProjects(String userName);

    public Project saveProject(Project project);

    public void deleteProject(Long id);

    public Project find(Long id);

    public Project findByCaseId(Long caseId);
}
