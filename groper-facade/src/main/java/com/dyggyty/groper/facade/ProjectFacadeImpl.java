package com.dyggyty.groper.facade;

import com.dyggyty.groper.bd.ProjectBd;
import com.dyggyty.groper.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author mobius
 */
@Service
public class ProjectFacadeImpl implements ProjectFacade {

    @Autowired
    private ProjectBd projectBd;

    @Override
    public List<Project> getUserProjects(String userName) {
        return projectBd.getUserProjects(userName);
    }

    @Override
    public Project saveProject(Project project) {
        return projectBd.save(project);
    }

    @Override
    public void deleteProject(Long id) {
        projectBd.delete(id);
    }

    @Override
    public Project find(Long id) {
        return projectBd.findOne(id);
    }

    @Override
    public Project findByCaseId(Long caseId) {
        return projectBd.findByCaseId(caseId);
    }
}
