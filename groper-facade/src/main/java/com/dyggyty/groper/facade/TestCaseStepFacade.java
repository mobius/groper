package com.dyggyty.groper.facade;

import com.dyggyty.groper.model.TestCaseStep;

import java.util.List;

/**
 * @author mobius
 */
public interface TestCaseStepFacade {

    public TestCaseStep findOne(Long testCaseStepId);

    public List<TestCaseStep> getTestCaseStepsForTestCase(Long testCaseId);

    public List<TestCaseStep> getTestCaseStepsForProject(Long projectId);

    public List<TestCaseStep> getTestCaseSteps(Long testCaseId);

    public TestCaseStep saveTestCaseStep(TestCaseStep testCaseStep);

    public void deleteTestCaseStep(Long testCaseStepId);
}
