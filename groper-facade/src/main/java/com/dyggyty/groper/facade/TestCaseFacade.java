package com.dyggyty.groper.facade;

import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import java.util.List;

/**
 * @author mobius
 */
public interface TestCaseFacade {

    public TestCase find(Long testCaseStepId);

    public TestCase saveTestCase(Project project, TestCase testCase);

    public void removeTestStep(Long testCaseId, Long testCaseStepId);

    public void deleteTestCase(Long testCaseId);

    public List<TestCase> getTestCases(Long projectId);

    public void setTestCaseResult(Long testCaseId, String testCaseResult);

    public void addTestCaseStep(Long testCaseId, Long testCaseStepId);

    public void moveStepUp(Long testCaseId, int position);

    public void moveStepDown(Long testCaseId, int position);
}
