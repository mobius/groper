package com.dyggyty.groper.facade;

import com.dyggyty.groper.bd.UserProfileBd;
import com.dyggyty.groper.model.EntityState;
import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.utils.PwdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author mobius
 */
@Service
public class UserProfileFacadeImpl implements UserProfileFacade {

    @Autowired
    private UserProfileBd userProfileBd;

    @Override
    public UserProfile authenticateUser(String userName, String userPassword) {
        UserProfile userProfile = userProfileBd.findOne(userName); //todo add check only latin letters
        if (userProfile == null) {
            return null;
        }

        PwdUtils.PwdDetails pwdDetails = PwdUtils.encryptPassword(userPassword, userProfile.getPasswordSalt());
        return userProfileBd.loginUser(userName, pwdDetails.getPwdHash());
    }

    @Override
    public Boolean signUp(UserProfile userProfile, String userPassword) {

        PwdUtils.PwdDetails pwdDetails = PwdUtils.encryptPassword(userPassword);
        userProfile.setPassword(pwdDetails.getPwdHash());
        userProfile.setPasswordSalt(pwdDetails.getSalt());
        userProfile.setEntityState(EntityState.ACTIVATE);

        return userProfileBd.registerUser(userProfile) != null;
    }
}
