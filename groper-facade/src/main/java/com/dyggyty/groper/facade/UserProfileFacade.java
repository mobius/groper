package com.dyggyty.groper.facade;

import com.dyggyty.groper.model.UserProfile;

/**
 * @author mobius
 */
public interface UserProfileFacade {

    public UserProfile authenticateUser(String userName, String userPassword);

    public Boolean signUp(UserProfile userProfile, String userPassword);
}
