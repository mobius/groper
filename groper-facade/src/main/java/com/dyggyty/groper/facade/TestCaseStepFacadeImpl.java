package com.dyggyty.groper.facade;

import com.dyggyty.groper.bd.TestCaseStepBd;
import com.dyggyty.groper.model.TestCaseStep;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author mobius
 */
@Service
public class TestCaseStepFacadeImpl implements TestCaseStepFacade {

    @Autowired
    private TestCaseStepBd testCaseStepBd;

    @Override
    public TestCaseStep findOne(Long testCaseStepId) {
        return testCaseStepBd.findOne(testCaseStepId);
    }

    @Override
    public List<TestCaseStep> getTestCaseStepsForTestCase(Long testCaseId) {
        return testCaseStepBd.getTestCaseStepsForTestCase(testCaseId);
    }

    @Override
    public List<TestCaseStep> getTestCaseStepsForProject(Long projectId) {
        return testCaseStepBd.getTestCaseStepsForProject(projectId);
    }

    @Override
    public List<TestCaseStep> getTestCaseSteps(Long testCaseId) {
        return testCaseStepBd.getTestCaseSteps(testCaseId);
    }

    @Override
    public TestCaseStep saveTestCaseStep(TestCaseStep testCaseStep) {
        return testCaseStepBd.save(testCaseStep);
    }

    @Override
    public void deleteTestCaseStep(Long testCaseStepId) {
        testCaseStepBd.delete(testCaseStepId);
    }
}
