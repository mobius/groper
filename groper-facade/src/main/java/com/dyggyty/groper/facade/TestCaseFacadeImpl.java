package com.dyggyty.groper.facade;

import com.dyggyty.groper.bd.TestCaseBd;
import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author mobius
 */
@Service
public class TestCaseFacadeImpl implements TestCaseFacade {

    @Autowired
    private TestCaseBd testCaseBd;

    @Override
    public TestCase find(Long testCaseStepId) {
        return testCaseBd.findOne(testCaseStepId);
    }

    @Override
    public TestCase saveTestCase(Project project, TestCase testCase) {
        if (project != null) {
            testCase.setProject(project);
        }
        return testCaseBd.save(testCase);
    }

    @Override
    public void removeTestStep(Long testCaseId, Long testCaseStepId) {
        testCaseBd.removeTestStep(testCaseId, testCaseStepId);
    }

    @Override
    public void deleteTestCase(Long testCaseId) {
        testCaseBd.delete(testCaseId);
    }

    @Override
    public List<TestCase> getTestCases(Long projectId) {
        return testCaseBd.getTestCases(projectId);
    }

    @Override
    public void setTestCaseResult(Long testCaseId, String testCaseResult) {
        testCaseBd.setTestCaseResult(testCaseId, testCaseResult);
    }

    @Override
    public void addTestCaseStep(Long testCaseId, Long testCaseStepId) {
        testCaseBd.addTestCaseStep(testCaseId, testCaseStepId);
    }

    @Override
    public void moveStepUp(Long testCaseId, int position) {
        testCaseBd.changeStepPosition(testCaseId, position, position - 1);
    }

    @Override
    public void moveStepDown(Long testCaseId, int position) {
        testCaseBd.changeStepPosition(testCaseId, position, position + 1);
    }
}
