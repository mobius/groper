package com.dyggyty.groper.bd;

import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;

import java.util.List;

/**
 * @author mobius
 */
public interface TestCaseBd extends GenericBd<Long, TestCase> {

    public List<TestCase> getTestCases(Project project);

    public List<TestCase> getTestCases(Long projectId);

    public void setTestCaseResult(Long testCaseId, String testCaseResult);

    public void removeTestStep(Long testCaseId, Long testCaseStepId);

    public void addTestCaseStep(Long testCaseId, Long testCaseStepId);

    public void changeStepPosition(Long testCaseId, int oldPosition, int newPosition);
}
