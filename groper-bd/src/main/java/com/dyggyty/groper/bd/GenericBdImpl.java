package com.dyggyty.groper.bd;

import com.dyggyty.groper.model.GenericEntity;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

/**
 * @author vitaly.rudenya
 */
public abstract class GenericBdImpl<ID extends Serializable, T extends GenericEntity> implements GenericBd<ID, T> {

    protected abstract CrudRepository<T, ID> getRepository();

    @Override
    public T save(T entity) {
        return getRepository().save(entity);
    }

    @Override
    public Iterable<T> save(Iterable<T> entities) {
        return getRepository().save(entities);
    }

    @Override
    public T findOne(ID id) {
        return getRepository().findOne(id);
    }

    @Override
    public boolean exists(ID id) {
        return getRepository().exists(id);
    }

    @Override
    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        return getRepository().findAll(ids);
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public void delete(ID id) {
        getRepository().delete(id);
    }

    @Override
    public void delete(T entity) {
        getRepository().delete(entity);
    }

    @Override
    public void delete(Iterable<T> entities) {
        getRepository().delete(entities);
    }

    @Override
    public void deleteAll() {
        getRepository().deleteAll();
    }
}
