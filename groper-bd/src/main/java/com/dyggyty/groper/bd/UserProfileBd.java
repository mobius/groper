package com.dyggyty.groper.bd;

import com.dyggyty.groper.model.UserProfile;

/**
 * @author mobius
 *         Date: 23.07.12
 *         Time: 22:27
 */
public interface UserProfileBd extends GenericBd<String, UserProfile> {

    /**
     * Check user name and password combination.
     *
     * @param userName User login name.
     * @param password Encrypted user password.
     * @return New user session ID or NULL if user name and password combination was not found.
     */
    public UserProfile loginUser(String userName, String password);

    /**
     * Register new user.
     *
     * @param userProfile User entity.
     * @return TRUE if registration was success, else FALSE.
     */
    public UserProfile registerUser(UserProfile userProfile);

    /**
     * Retrieve user by login name.
     *
     * @param userSession User session value.
     * @return Existing user object.
     */
    public UserProfile getUserBySession(String userSession);

    /**
     * Retrieve user password salt
     *
     * @param userName User login name
     * @return Salt
     */
    public String getUserSalt(String userName);

    /**
     * Delete current user.
     *
     * @param userSession Current user session.
     */
    public void deleteCurrentUser(String userSession);
}
