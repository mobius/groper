package com.dyggyty.groper.bd;

import com.dyggyty.groper.dao.ProjectRepository;
import com.dyggyty.groper.model.Project;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author mobius
 */
@Transactional
@Component
public class ProjectBdImpl extends GenericBdImpl<Long, Project> implements ProjectBd {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    protected CrudRepository<Project, Long> getRepository() {
        return projectRepository;
    }

    @Override
    public List<Project> getUserProjects(String userName) {
        return projectRepository.getUserProjects(userName);
    }

    @Override
    public Project findByCaseId(Long caseId) {
        return projectRepository.findByCaseId(caseId);
    }
}
