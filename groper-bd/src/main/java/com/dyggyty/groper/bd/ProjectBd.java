package com.dyggyty.groper.bd;

import com.dyggyty.groper.model.Project;
import java.util.List;

/**
 * @author mobius
 */
public interface ProjectBd extends GenericBd<Long, Project> {

    public List<Project> getUserProjects(String userName);

    public Project findByCaseId(Long caseId);
}
