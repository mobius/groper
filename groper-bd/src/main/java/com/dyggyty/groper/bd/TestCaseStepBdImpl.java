package com.dyggyty.groper.bd;

import com.dyggyty.groper.dao.TestCaseRepository;
import com.dyggyty.groper.dao.TestCaseStepRepository;
import com.dyggyty.groper.model.TestCaseStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author mobius
 */
@Transactional
@Component
public class TestCaseStepBdImpl extends GenericBdImpl<Long, TestCaseStep> implements TestCaseStepBd {

    @Autowired
    private TestCaseStepRepository testCaseStepRepository;
    @Autowired
    private TestCaseRepository testCaseRepository;

    @Override
    protected CrudRepository<TestCaseStep, Long> getRepository() {
        return testCaseStepRepository;
    }

    @Override
    public List<TestCaseStep> getTestCaseSteps(Long testCaseId) {
        List<TestCaseStep> testCaseSteps = testCaseRepository.findOne(testCaseId).getCaseSteps();
        testCaseSteps.size();
        return testCaseSteps;
    }

    @Override
    public List<TestCaseStep> getTestCaseStepsForProject(Long projectId) {
        return testCaseStepRepository.getTestCaseStepsForProject(projectId);
    }

    @Override
    public List<TestCaseStep> getTestCaseStepsForTestCase(Long testCaseId) {
        return testCaseStepRepository.getTestCaseStepsForTestCase(testCaseId);
    }
}
