package com.dyggyty.groper.bd;

import com.dyggyty.groper.model.TestCaseStep;

import java.util.List;

/**
 * @author mobius
 */
public interface TestCaseStepBd extends GenericBd<Long, TestCaseStep> {

    public List<TestCaseStep> getTestCaseSteps(Long testCaseId);

    public List<TestCaseStep> getTestCaseStepsForProject(Long projectId);

    public List<TestCaseStep> getTestCaseStepsForTestCase(Long testCaseId);
}
