package com.dyggyty.groper.bd;

import com.dyggyty.groper.dao.TestCaseRepository;
import com.dyggyty.groper.dao.TestCaseStepRepository;
import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import com.dyggyty.groper.model.TestCaseStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

/**
 * @author mobius
 */
@Transactional
@Component
public class TestCaseBdImpl extends GenericBdImpl<Long, TestCase> implements TestCaseBd {

    @Autowired
    private TestCaseRepository testCaseRepository;
    @Autowired
    private TestCaseStepRepository testCaseStepRepository;

    @Override
    protected CrudRepository<TestCase, Long> getRepository() {
        return testCaseRepository;
    }

    @Override
    public List<TestCase> getTestCases(Project project) {
        return testCaseRepository.getTestCases(project);
    }

    @Override
    public List<TestCase> getTestCases(Long projectId) {
        return testCaseRepository.getTestCases(projectId);
    }

    @Override
    public void setTestCaseResult(Long testCaseId, String testCaseResult) {
        testCaseRepository.setTestCaseResult(testCaseId, testCaseResult);
    }

    @Override
    public void removeTestStep(Long testCaseId, Long testCaseStepId) {
        TestCase testCase = testCaseRepository.findOne(testCaseId);

        TestCaseStep testCaseStepToRemove = null;
        for (TestCaseStep testCaseStep : testCase.getCaseSteps()) {
            if (testCaseStep == null || testCaseStepId.equals(testCaseStep.getId())) {
                testCaseStepToRemove = testCaseStep;
                break;
            }
        }

        if (testCaseStepToRemove != null) {
            testCase.getCaseSteps().remove(testCaseStepToRemove);
        }

        testCaseRepository.save(testCase);
    }

    @Override
    public void addTestCaseStep(Long testCaseId, Long testCaseStepId) {
        TestCase testCase = testCaseRepository.findOne(testCaseId);
        TestCaseStep testCaseStep = testCaseStepRepository.findOne(testCaseStepId);

        testCase.getCaseSteps().add(testCaseStep);

        testCaseRepository.save(testCase);
    }

    @Override
    public void changeStepPosition(Long testCaseId, int position, int newPosition) {
        TestCase testCase = testCaseRepository.findOne(testCaseId);

        List<TestCaseStep> testCaseSteps = testCase.getCaseSteps();
        if (position > 0 && position < testCaseSteps.size() && newPosition > 0 && newPosition < testCaseSteps.size()) {
            Collections.swap(testCaseSteps, position, newPosition);
        }
        testCaseRepository.save(testCase);
    }
}
