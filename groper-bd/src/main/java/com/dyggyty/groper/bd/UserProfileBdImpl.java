package com.dyggyty.groper.bd;

import com.dyggyty.groper.dao.UserProfileRepository;
import com.dyggyty.groper.model.EntityState;
import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.utils.ErrorMessages;
import com.dyggyty.groper.utils.MessageException;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: vitaly.rudenya
 * Date: 23.07.12
 * Time: 22:28
 */
@Transactional
@Component
public class UserProfileBdImpl extends GenericBdImpl<String, UserProfile> implements UserProfileBd {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Override
    protected CrudRepository<UserProfile, String> getRepository() {
        return userProfileRepository;
    }

    @Override
    public UserProfile loginUser(String userName, String password) {

        String uuid;
        UserProfile userProfile =
                userProfileRepository.getUserByNameAndPassword(userName, password, EntityState.ACTIVATE);
        if (userProfile == null) {
            return null;
        }

        uuid = UUID.randomUUID().toString();
        while (userProfileRepository.getUserBySession(uuid) != null) {
            uuid = UUID.randomUUID().toString();
        }

        userProfile.setUserSession(uuid);
        userProfile.setLastLogin(new Date());
        userProfileRepository.save(userProfile);

        return userProfile;
    }

    @Override
    public UserProfile registerUser(UserProfile userProfile) {
        boolean status = !userProfileRepository.exists(userProfile.getUserName());
        if (!status) {
            throw new MessageException(ErrorMessages.USER_ALREADY_EXISTS);
        }

        return userProfileRepository.save(userProfile);
    }

    @Override
    public UserProfile getUserBySession(String userSession) {
        return userProfileRepository.getUserBySession(userSession);
    }

    @Override
    public String getUserSalt(String userName) {
        return userProfileRepository.getUserSalt(userName);
    }

    @Transactional
    @Override
    public void deleteCurrentUser(String userSession) {
        UserProfile currentUserProfile = userProfileRepository.getUserBySession(userSession);
        currentUserProfile.setEntityState(EntityState.TERMINATED);
        userProfileRepository.save(currentUserProfile);
    }
}
