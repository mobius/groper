package com.dyggyty.groper.utils;

import org.mindrot.jbcrypt.BCrypt;

/**
 * @author mobius
 */
public class PwdUtils {

    private PwdUtils() {
    }

    public static PwdDetails encryptPassword(String password) {
        return encryptPassword(password, null);
    }

    public static PwdDetails encryptPassword(String password, String salt) {
        if (password == null) {
            throw new IllegalArgumentException("Password can't be null");
        }

        String pwdSalt;
        if (salt == null || salt.length() == 0) {
            pwdSalt = BCrypt.gensalt();
        } else {
            pwdSalt = salt;
        }

        String hashed = BCrypt.hashpw(password, pwdSalt);

        return new PwdDetails(hashed, pwdSalt);
    }

    public static final class PwdDetails {
        private String salt;
        private String pwdHash;

        private PwdDetails(String pwdHash, String salt) {
            this.pwdHash = pwdHash;
            this.salt = salt;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getPwdHash() {
            return pwdHash;
        }

        public void setPwdHash(String pwdHash) {
            this.pwdHash = pwdHash;
        }
    }
}
