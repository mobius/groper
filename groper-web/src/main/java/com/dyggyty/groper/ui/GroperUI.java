package com.dyggyty.groper.ui;

import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

@Title("Groper")
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GroperUI extends UI implements ApplicationContextAware {

    private static final String REQUEST_LANGUAGE_PARAMETER = "language";
    private static final String SESSION_CURRENT_LOCALE = "locale";

    @Autowired
    private LoginView loginView;
    @Autowired
    private LocaleResolver localeResolver;

    private ApplicationContext applicationContext;
    private Locale currentLocale;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public String getMessage(String key) {
        return applicationContext.getMessage(key, null, currentLocale);
    }

    /**
     * After UI class is created, init() is executed. You should build and wire
     * up your user interface here.
     */
    protected void init(VaadinRequest request) {

        VaadinServletRequest vsRequest = (VaadinServletRequest) request;

        VaadinSession session = getSession();
        String language = request.getParameter(REQUEST_LANGUAGE_PARAMETER);
        if (language != null) {
            session.setAttribute(SESSION_CURRENT_LOCALE, new Locale(language));
        }

        currentLocale = (Locale) session.getAttribute(SESSION_CURRENT_LOCALE);
        if (currentLocale == null) {
            currentLocale = localeResolver.resolveLocale(vsRequest.getHttpServletRequest());
            session.setAttribute(SESSION_CURRENT_LOCALE, currentLocale);
        }

        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setSizeFull();
        setContent(layout);

        Navigator.ComponentContainerViewDisplay viewDisplay = new Navigator.ComponentContainerViewDisplay(layout);
        Navigator navigator = new Navigator(UI.getCurrent(), viewDisplay);
        navigator.addView("", loginView);

        Map<String, View> views = BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, View.class);
        for (Map.Entry<String, View> entry : views.entrySet()) {
            if (!entry.getValue().getClass().equals(loginView.getClass())) {
                navigator.addView(entry.getKey(), entry.getValue());
            }
        }
    }
}
