package com.dyggyty.groper.ui.editor;

import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import org.vaadin.aceeditor.AceEditor;

/**
 * @author mobius
 */
public class LeetSpeakerizer implements TextChangeListener {

    private AceEditor editor;

    public void attachTo(AceEditor editor) {
        this.editor = editor;
        editor.addTextChangeListener(this);
        correctText(editor.getValue());
    }

    @Override
    public void textChange(TextChangeEvent event) {
        correctText(event.getText());
    }

    private void correctText(String text) {
        if (text != null && text.length() > 0 && text.length() > editor.getCursorPosition() &&
                text.charAt(editor.getCursorPosition()) == '{') {
            int current = editor.getCursorPosition() + 1;
            int newPosition = current + 1;
            String newText = text.substring(0, current) + "\n\n}" + text.substring(newPosition);

            editor.setValue(newText);
            editor.setCursorPosition(newPosition);
        }
    }
}
