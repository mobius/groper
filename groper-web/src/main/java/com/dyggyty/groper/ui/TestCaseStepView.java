package com.dyggyty.groper.ui;

import com.dyggyty.groper.facade.ProjectFacade;
import com.dyggyty.groper.facade.TestCaseStepFacade;
import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCaseStep;
import com.dyggyty.groper.ui.editor.LeetSpeakerizer;
import com.dyggyty.groper.ui.editor.ScriptErrorChecker;
import com.dyggyty.groper.ui.editor.ScriptSuggester;
import com.dyggyty.groper.worker.GroperScriptableObject;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.aceeditor.SuggestionExtension;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestCaseStepView extends ListView<TestCaseStep> implements ApplicationContextAware {

    private static final String VIEW_HEADING = "Test Case Step";

    private static final String ID_CREATED = "created";

    private static final String TITLE_ID = "#";
    private static final String TITLE_CREATED = "Created";

    //project test case step fields
    private static final String ID_PROJECT_CASE_STEP_ID = "id";
    private static final String ID_PROJECT_CASE_STEP_CASE = "testCase";
    private static final String ID_PROJECT_CASE_STEP_NAME = "caseStepName";
    private static final String ID_PROJECT_CASE_STEP_DESC = "description";
    private static final String ID_PROJECT_CASE_STEP_SCRIPT = "script";

    private static final String PROJECT_CASE_STEP_NAME = "Case Step Name";
    private static final String PROJECT_CASE_STEP_DESC = "Description";

    public static final String[] PARENT_LIST_VISIBLE_COLUMNS = {ID_PROJECT_CASE_STEP_ID, ID_PROJECT_CASE_STEP_NAME,
            ID_CREATED};
    public static final Map<String, String> PARENT_LIST_HEADERS;

    static {
        Map<String, String> parentListHeaders = new HashMap<>();

        parentListHeaders.put(ID_PROJECT_CASE_STEP_NAME, TITLE_NAME);
        parentListHeaders.put(ID_PROJECT_CASE_STEP_ID, TITLE_ID);
        parentListHeaders.put(ID_CREATED, TITLE_CREATED);

        PARENT_LIST_HEADERS = Collections.unmodifiableMap(parentListHeaders);
    }

    private ApplicationContext applicationContext;

    @Autowired
    private ProjectFacade projectFacade;
    @Autowired
    private TestCaseStepFacade testCaseStepFacade;

    @Override
    public String getHeading() {
        return VIEW_HEADING;
    }

    @Override
    protected TestCaseStep createEmptyParent() {

        Long caseId = getLongParameter(GenericView.REQUEST_TEST_CASE_ID);
        Project project = projectFacade.findByCaseId(caseId);

        TestCaseStep testCaseStep = new TestCaseStep();
        testCaseStep.setCaseStepName("New Test Case Step");
        testCaseStep.setProject(project);

        return testCaseStep;
    }

    @Override
    protected TestCaseStep saveParent(TestCaseStep testCaseStep) {
        return testCaseStepFacade.saveTestCaseStep(testCaseStep);
    }

    @Override
    protected void deleteParent(Long parentId) {
        testCaseStepFacade.deleteTestCaseStep(parentId);
    }

    @Override
    protected BeanContainer<Long, TestCaseStep> createParentDataSource() {
        BeanContainer<Long, TestCaseStep> ic = new BeanContainer<>(TestCaseStep.class);

        ic.setBeanIdProperty(ID_PROJECT_CASE_STEP_ID);
        ic.removeContainerProperty(ID_PROJECT_CASE_STEP_CASE);

        return ic;
    }

    @Override
    protected List<TestCaseStep> getParentsList() {
        if (hasParameter(GenericView.REQUEST_TEST_CASE_ID)) {
            return testCaseStepFacade.getTestCaseSteps(getLongParameter(GenericView.REQUEST_TEST_CASE_ID));
        }

        return testCaseStepFacade.getTestCaseStepsForProject(getLongParameter(GenericView.REQUEST_PROJECT_ID));
    }

    @Override
    protected String getFilteredFieldId() {
        return ID_PROJECT_CASE_STEP_NAME;
    }

    @Override
    protected Map<String, String> getParentListHeaders() {
        return PARENT_LIST_HEADERS;
    }

    @Override
    protected String[] getVisibleColumns() {
        return PARENT_LIST_VISIBLE_COLUMNS;
    }

    @Override
    protected Map<String, AbstractField> createEditorFields() {

        Map<String, AbstractField> fields = new LinkedHashMap<>();

        TextField nameTextField = new TextField(PROJECT_CASE_STEP_NAME);
        nameTextField.setWidth(100, Unit.PERCENTAGE);
        fields.put(ID_PROJECT_CASE_STEP_NAME, nameTextField);

        TextArea projectDescriptionArea = new TextArea(PROJECT_CASE_STEP_DESC);
        projectDescriptionArea.setSizeFull();
        fields.put(ID_PROJECT_CASE_STEP_DESC, projectDescriptionArea);

        AceEditor jsEditor = new AceEditor();
        jsEditor.setSizeFull();
        jsEditor.setHeight(400, Unit.PIXELS);
        new LeetSpeakerizer().attachTo(jsEditor);

        List<GroperScriptableObject> scriptableObjectList =
                new ArrayList<>(BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, GroperScriptableObject.class).values());
        new SuggestionExtension(new ScriptSuggester(scriptableObjectList)).extend(jsEditor);
        new ScriptErrorChecker().attachTo(jsEditor);

        fields.put(ID_PROJECT_CASE_STEP_SCRIPT, jsEditor);

        return fields;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
