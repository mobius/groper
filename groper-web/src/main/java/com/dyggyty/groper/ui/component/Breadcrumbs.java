package com.dyggyty.groper.ui.component;

import com.vaadin.navigator.Navigator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vitaly.rudenya
 */
public class Breadcrumbs implements Serializable {

    private final List<BreadcrumbTrail> breadcrumbTrails = new LinkedList<>();
    private final Set<String> viewNames = new HashSet<>();

    public synchronized void navigate(String viewName, String title, Map<String, String> parameters, boolean topFlag) {

        if (topFlag && breadcrumbTrails.size() > 0) {
            if (breadcrumbTrails.get(0).getViewName().equals(viewName)) {
                if (breadcrumbTrails.size() > 1) {
                    List<BreadcrumbTrail> breadcrumbTrails1ToRemove = new ArrayList<>(breadcrumbTrails
                            .subList(1, breadcrumbTrails.size()));
                    removeTrails(breadcrumbTrails1ToRemove);
                }
            } else {
                breadcrumbTrails.clear();
                viewNames.clear();
            }
        }

        if (hasView(viewName)) {
            List<BreadcrumbTrail> breadcrumbTrails1ToRemove = new ArrayList<>();
            boolean existsFlag = false;
            for (BreadcrumbTrail breadcrumbTrail : breadcrumbTrails) {
                if (existsFlag) {
                    breadcrumbTrails1ToRemove.add(breadcrumbTrail);
                } else if (breadcrumbTrail.getViewName().equals(viewName)) {
                    existsFlag = true;
                }
            }

            removeTrails(breadcrumbTrails1ToRemove);
        } else {
            breadcrumbTrails.add(new BreadcrumbTrail(viewName, title, parameters));
            viewNames.add(viewName);
        }
    }

    public void stepBack(Navigator navigator) {
        if (breadcrumbTrails.size() > 1) {
            BreadcrumbTrail previousTrail = breadcrumbTrails.get(breadcrumbTrails.size() - 2);
            navigator.navigateTo(createNavigationPath(previousTrail));
        }
    }

    public void stepTo(String viewName, Navigator navigator) {
        for (BreadcrumbTrail breadcrumbTrail : breadcrumbTrails) {
            if (breadcrumbTrail.getViewName().equals(viewName)) {
                navigator.navigateTo(createNavigationPath(breadcrumbTrail));
            }
        }
    }

    public List<BreadcrumbTrail> getBreadcrumbTrails() {
        return breadcrumbTrails.subList(0, breadcrumbTrails.size());
    }

    public boolean hasView(String viewName) {
        return viewNames.contains(viewName);
    }

    private void removeTrails(List<BreadcrumbTrail> breadcrumbTrails1ToRemove) {
        breadcrumbTrails.removeAll(breadcrumbTrails1ToRemove);
        for (BreadcrumbTrail breadcrumbTrail : breadcrumbTrails1ToRemove) {
            viewNames.remove(breadcrumbTrail.getViewName());
        }
    }

    private String createNavigationPath(BreadcrumbTrail trail) {
        StringBuilder path = new StringBuilder(trail.getViewName());
        Map<String, String> parameters = trail.getParameters();
        if (parameters != null) {
            for (Map.Entry<String, String> parameterEntry : parameters.entrySet()) {
                path.append("/").append(parameterEntry.getKey()).append(",").append(parameterEntry.getValue());
            }
        }
        return path.toString();
    }

    /**
     * @author vitaly.rudenya
     */
    public static class BreadcrumbTrail implements Serializable {

        private String viewName;
        private String title;
        private Map<String, String> parameters;

        private BreadcrumbTrail(String viewName, String title, Map<String, String> parameters) {
            this.viewName = viewName;
            this.title = title;
            this.parameters = parameters;
        }

        public String getViewName() {
            return viewName;
        }

        public String getTitle() {
            return title;
        }

        public Map<String, String> getParameters() {
            return parameters;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            BreadcrumbTrail that = (BreadcrumbTrail) o;

            return viewName.equals(that.viewName);
        }

        @Override
        public int hashCode() {
            return viewName.hashCode();
        }
    }
}
