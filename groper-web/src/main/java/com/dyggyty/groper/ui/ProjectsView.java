package com.dyggyty.groper.ui;

import com.dyggyty.groper.facade.ProjectFacade;
import com.dyggyty.groper.facade.TestCaseFacade;
import com.dyggyty.groper.facade.TestCaseStepFacade;
import com.dyggyty.groper.model.GenericEntity;
import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import com.dyggyty.groper.model.TestCaseStep;
import com.dyggyty.groper.ui.component.MenuItemView;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProjectsView extends ChildAwareListView<Project, GenericEntity> implements MenuItemView {

    private static final String VIEW_HEADING = "Projects";

    //project fields
    private static final String ID_PROJECT_ID = "id";
    private static final String ID_PROJECT_NAME = "projectName";
    private static final String ID_PROJECT_DESC = "projectDescription";
    private static final String ID_OWNER = "owner";
    private static final String ID_CREATED = "created";

    //project test case fields
    private static final String ID_PROJECT_CASE_STEP_ID = "id";
    private static final String ID_PROJECT_CASE_ID = "id";
    private static final String ID_CASE_STEPS = "testCaseSteps";
    private static final String ID_PROJECT_CASE_STEP_CASE = "testCase";

    private static final String LABEL_TEST_CASES_TAB_TITLE = "Test Cases";
    private static final String LABEL_TEST_STEPS_TAB_TITLE = "Test Steps";
    private static final String[] TAB_TITLES = {LABEL_TEST_CASES_TAB_TITLE, LABEL_TEST_STEPS_TAB_TITLE};

    private static final String[] PARENT_LIST_VISIBLE_COLUMNS = {ID_PROJECT_ID, ID_PROJECT_NAME, ID_CREATED};
    private static final Map<String, String> PARENT_LIST_HEADERS;

    static {
        Map<String, String> parentListHeaders = new HashMap<>();

        parentListHeaders.put(ID_PROJECT_NAME, TITLE_NAME);
        parentListHeaders.put(ID_PROJECT_ID, TITLE_ID);
        parentListHeaders.put(ID_CREATED, TITLE_CREATED);

        PARENT_LIST_HEADERS = Collections.unmodifiableMap(parentListHeaders);
    }

    @Autowired
    private ProjectFacade projectFacade;
    @Autowired
    private TestCaseFacade testCaseFacade;
    @Autowired
    private TestCaseStepFacade testCaseStepFacade;

    @Override
    public String getHeading() {
        return VIEW_HEADING;
    }

    @Override
    public int getOrder() {
        return 100;
    }

    @Override
    protected Project createEmptyParent() {

        Project project = new Project();
        project.setProjectName("New Project");
        project.setOwner(getCurrentUserProfile());

        return project;
    }

    @Override
    protected Project saveParent(Project project) {
        return projectFacade.saveProject(project);
    }

    @Override
    protected void deleteParent(Long parentId) {
        projectFacade.deleteProject(parentId);
    }

    protected BeanContainer<Long, Project> createParentDataSource() {
        BeanContainer<Long, Project> ic = new BeanContainer<>(Project.class);

        ic.setBeanIdProperty(ID_PROJECT_ID);
        ic.removeContainerProperty(ID_OWNER);

        return ic;
    }

    @Override
    protected String getChildIdName(int tabInd) {
        switch (tabInd) {
            case 0: {
                return GenericView.REQUEST_TEST_CASE_ID;
            }

            case 1: {
                return GenericView.REQUEST_TEST_CASE_STEP_ID;
            }
        }
        return null;
    }

    @Override
    protected String getParentIdName() {
        return GenericView.REQUEST_PROJECT_ID;
    }

    @Override
    protected int getTabsCount() {
        return 2;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BeanContainer<Long, GenericEntity> createChildDataSource(int tabInd) {
        switch (tabInd) {
            case 0: {
                BeanContainer ic = new BeanContainer(TestCase.class);

                ic.setBeanIdProperty(ID_PROJECT_CASE_ID);
                ic.removeContainerProperty(ID_CASE_STEPS);

                return ic;
            }

            case 1: {
                BeanContainer ic = new BeanContainer(TestCaseStep.class);

                ic.setBeanIdProperty(ID_PROJECT_CASE_STEP_ID);
                ic.removeContainerProperty(ID_PROJECT_CASE_STEP_CASE);

                return ic;
            }
        }

        return null;
    }

    @Override
    protected List<Project> getParentsList() {
        return projectFacade.getUserProjects(getCurrentUserProfile().getUserName());
    }

    @Override
    protected List<GenericEntity> getChildren(int tabInd, Long parentId) {
        List<GenericEntity> children = new ArrayList<>();
        switch (tabInd) {
            case 0: {
                children.addAll(testCaseFacade.getTestCases(parentId));
                break;
            }

            case 1: {
                children.addAll(testCaseStepFacade.getTestCaseStepsForProject(parentId));
                break;
            }
        }

        return children;
    }

    @Override
    protected String getFilteredFieldId() {
        return ID_PROJECT_NAME;
    }

    @Override
    protected Map<String, String> getParentListHeaders() {
        return PARENT_LIST_HEADERS;
    }

    @Override
    protected String[] getVisibleColumns() {
        return PARENT_LIST_VISIBLE_COLUMNS;
    }

    @Override
    protected Map<String, String> getChildListHeaders(int tabInd) {
        switch (tabInd) {
            case 0: {
                return TestCaseView.PARENT_LIST_HEADERS;
            }

            case 1: {
                return TestCaseStepView.PARENT_LIST_HEADERS;
            }
        }

        return null;
    }

    @Override
    protected String[] getChildVisibleColumns(int tabInd) {
        switch (tabInd) {
            case 0: {
                return TestCaseView.PARENT_LIST_VISIBLE_COLUMNS;
            }

            case 1: {
                return TestCaseStepView.PARENT_LIST_VISIBLE_COLUMNS;
            }
        }

        return null;
    }

    @Override
    protected String getChildrenTabTitle(int tabInd) {
        return TAB_TITLES[tabInd];
    }

    @Override
    protected Class<? extends AuthenticatedView> getChildrenClassView(int tabInd) {
        switch (tabInd) {
            case 0: {
                return TestCaseView.class;
            }

            case 1: {
                return TestCaseStepView.class;
            }
        }

        return null;
    }

    @Override
    protected Map<String, AbstractField> createEditorFields() {
        Map<String, AbstractField> fields = new LinkedHashMap<>();

        TextField projectNameTextField = new TextField(TITLE_NAME);
        projectNameTextField.setWidth(100, Unit.PERCENTAGE);
        fields.put(ID_PROJECT_NAME, projectNameTextField);

        TextArea projectDescriptionArea = new TextArea(TITLE_DESC);
        projectDescriptionArea.setSizeFull();
        fields.put(ID_PROJECT_DESC, projectDescriptionArea);

        return fields;
    }
}
