package com.dyggyty.groper.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.VerticalLayout;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mobius
 */
public abstract class GenericView extends VerticalLayout implements View {

    public static final String REQUEST_PROJECT_ID = "projectId";
    public static final String REQUEST_TEST_CASE_ID = "testCaseId";
    public static final String REQUEST_TEST_CASE_STEP_ID = "testCaseStepId";

    public static final String LABEL_SIGN_UP = "label.signUp";

    private final Map<String, String> parameters = new HashMap<>();

    protected String getMessage(String key) {
        return ((GroperUI) getUI()).getMessage(key);
    }

    public void enter(ViewChangeListener.ViewChangeEvent e) {
        parameters.clear();

        String[] parametersPairs = e.getParameters().split("/");
        for (String parameterPair : parametersPairs) {
            if (!StringUtils.isEmpty(parameterPair)) {
                String[] parameter = parameterPair.split(",");
                parameters.put(parameter[0], parameter.length > 1 ? parameter[1] : Boolean.TRUE.toString());
            }
        }
    }

    protected String getParameter(String paramKey) {
        return parameters.get(paramKey);
    }
}
