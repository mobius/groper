package com.dyggyty.groper.ui;

import com.dyggyty.groper.facade.ProjectFacade;
import com.dyggyty.groper.facade.TestCaseFacade;
import com.dyggyty.groper.facade.TestCaseStepFacade;
import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import com.dyggyty.groper.model.TestCaseStep;
import com.dyggyty.groper.worker.processor.Worker;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;

/**
 * @author mobius
 */
@org.springframework.stereotype.Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestCaseView extends ChildAwareListView<TestCase, TestCaseStep> implements ApplicationContextAware {

    private static final String VIEW_HEADING = "Test Case";

    private static final int TEST_STEPS_TAB_IND = 0;

    //project test case fields
    private static final String ID_CREATED = "created";
    private static final String ID_PROJECT_CASE_ID = "id";
    private static final String ID_PROJECT_CASE_NAME = "caseName";
    private static final String ID_PROJECT_CASE_DESC = "caseDescription";
    private static final String ID_CASE_STEPS = "testCaseSteps";
    private static final String ID_PROJECT_CASE_LAST_RESULT = "lastResult";

    //project test case step fields
    private static final String ID_PROJECT_CASE_STEP_ID = "id";
    private static final String ID_PROJECT_CASE_STEP_CASE = "testCase";
    private static final String ID_PROJECT_CASE_STEP_NAME = "caseStepName";

    private static final String LABEL_CHILDREN_TAB_TITLE = "Test Case Steps";
    private static final String LABEL_LAST_RESULT = "Last Result";

    public static final String[] PARENT_LIST_VISIBLE_COLUMNS = {ID_PROJECT_CASE_ID, ID_PROJECT_CASE_NAME, ID_CREATED};
    public static final Map<String, String> PARENT_LIST_HEADERS;

    @Autowired
    private TestCaseFacade testCaseFacade;
    @Autowired
    private ProjectFacade projectFacade;
    @Autowired
    private TestCaseStepFacade testCaseStepFacade;

    private ApplicationContext applicationContext;

    static {
        Map<String, String> parentListHeaders = new HashMap<>();

        parentListHeaders.put(ID_PROJECT_CASE_NAME, TITLE_NAME);
        parentListHeaders.put(ID_PROJECT_CASE_ID, TITLE_ID);
        parentListHeaders.put(ID_CREATED, TITLE_CREATED);

        PARENT_LIST_HEADERS = Collections.unmodifiableMap(parentListHeaders);
    }

    private static final String[] CHILD_LIST_VISIBLE_COLUMNS = {ID_PROJECT_CASE_STEP_ID, ID_PROJECT_CASE_STEP_NAME,
            ID_CREATED};
    private static final Map<String, String> CHILD_LIST_HEADERS;

    static {
        Map<String, String> parentListHeaders = new HashMap<>();

        parentListHeaders.put(ID_PROJECT_CASE_STEP_NAME, TITLE_NAME);
        parentListHeaders.put(ID_PROJECT_CASE_STEP_ID, TITLE_ID);
        parentListHeaders.put(ID_CREATED, TITLE_CREATED);

        CHILD_LIST_HEADERS = Collections.unmodifiableMap(parentListHeaders);
    }

    private Project project;

    private volatile Button removeButton;
    private volatile Button upButton;
    private volatile Button downButton;
    private volatile Button addButton;

    private final ComboBox availableSteps = new ComboBox();

    @Override
    public String getHeading() {
        return VIEW_HEADING;
    }

    protected void initData(){
        super.initData();
        project = projectFacade.find(Long.valueOf(getParameter(GenericView.REQUEST_PROJECT_ID)));
    }

    @Override
    protected int getTabsCount() {
        return 1;
    }

    @Override
    protected BeanContainer<Long, TestCaseStep> createChildDataSource(int tabInd) {
        BeanContainer<Long, TestCaseStep> ic = new BeanContainer<>(TestCaseStep.class);

        ic.setBeanIdProperty(ID_PROJECT_CASE_STEP_ID);
        ic.removeContainerProperty(ID_PROJECT_CASE_STEP_CASE);

        return ic;
    }

    @Override
    protected Collection<TestCaseStep> getChildren(int tabInd, Long parentId) {
        return testCaseStepFacade.getTestCaseSteps(parentId);
    }

    @Override
    protected Map<String, String> getChildListHeaders(int tabInd) {
        return CHILD_LIST_HEADERS;
    }

    @Override
    protected String[] getChildVisibleColumns(int tabInd) {
        return CHILD_LIST_VISIBLE_COLUMNS;
    }

    @Override
    protected String getChildrenTabTitle(int tabInd) {
        return LABEL_CHILDREN_TAB_TITLE;
    }

    @Override
    protected Class<? extends AuthenticatedView> getChildrenClassView(int tabInd) {
        return TestCaseStepView.class;
    }

    @Override
    protected TestCase createEmptyParent() {
        TestCase testCase = new TestCase();
        testCase.setCaseName("New Test Case");
        testCase.setProject(project);

        return testCase;
    }

    @Override
    protected TestCase saveParent(TestCase testCase) {
        return testCaseFacade.saveTestCase(project, testCase);
    }

    @Override
    protected void deleteParent(Long parentId) {
        testCaseFacade.deleteTestCase(parentId);
    }

    @Override
    protected BeanContainer<Long, TestCase> createParentDataSource() {
        BeanContainer<Long, TestCase> ic = new BeanContainer<>(TestCase.class);

        ic.setBeanIdProperty(ID_PROJECT_CASE_ID);
        ic.removeContainerProperty(ID_CASE_STEPS);

        return ic;
    }

    @Override
    protected List<TestCase> getParentsList() {
        return testCaseFacade.getTestCases(Long.valueOf(getParameter(GenericView.REQUEST_PROJECT_ID)));
    }

    @Override
    protected String getFilteredFieldId() {
        return ID_PROJECT_CASE_NAME;
    }

    @Override
    protected Map<String, String> getParentListHeaders() {
        return PARENT_LIST_HEADERS;
    }

    @Override
    protected String[] getVisibleColumns() {
        return PARENT_LIST_VISIBLE_COLUMNS;
    }

    @Override
    protected Map<String, AbstractField> createEditorFields() {
        Map<String, AbstractField> fields = new LinkedHashMap<>();

        TextField projectNameTextField = new TextField(TITLE_NAME);
        projectNameTextField.setWidth(100, Unit.PERCENTAGE);
        projectNameTextField.setRequired(true);
        fields.put(ID_PROJECT_CASE_NAME, projectNameTextField);

        TextArea projectDescriptionArea = new TextArea(TITLE_DESC);
        projectDescriptionArea.setSizeFull();
        fields.put(ID_PROJECT_CASE_DESC, projectDescriptionArea);

        TextArea lastResultArea = new TextArea(LABEL_LAST_RESULT);
        lastResultArea.setSizeFull();
        lastResultArea.setReadOnly(true);
        lastResultArea.setEnabled(false);
        fields.put(ID_PROJECT_CASE_LAST_RESULT, lastResultArea);

        return fields;
    }

    @Override
    protected List<Component> createChildListToolbarItems(int tabInd) {

        List<Component> toolBarComponents = super.createChildListToolbarItems(tabInd);

        if (TEST_STEPS_TAB_IND == tabInd) {
            toolBarComponents.add(getRemoveButton(tabInd));
            toolBarComponents.add(getUpButton(tabInd));
            toolBarComponents.add(getDownButton(tabInd));

            HorizontalLayout addLayout = new HorizontalLayout();
            addLayout.addComponent(getAddButton(tabInd));

            availableSteps.setItemCaptionMode(AbstractSelect.ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
            addLayout.addComponent(availableSteps);

            toolBarComponents.add(addLayout);
        }

        return toolBarComponents;
    }

    private Button getRemoveButton(final int tabInd) {

        if (removeButton == null) {
            synchronized (this) {
                if (removeButton == null) {
                    removeButton = new Button("Remove");
                    removeButton.setEnabled(false);

                    removeButton.addClickListener(new Button.ClickListener() {
                        @SuppressWarnings("unchecked")
                        public void buttonClick(Button.ClickEvent event) {
                            Table stepsTable = getCurrentChildesTable(tabInd);

                            Object testStepId = stepsTable.getValue();
                            if (testStepId == null) {
                                Notification.show("Please select test step for removal",
                                        Notification.Type.HUMANIZED_MESSAGE);
                                return;
                            }
                            testCaseFacade.removeTestStep(getSelectedParentId(), (Long) testStepId);
                            stepsTable.removeItem(testStepId);

                            TestCaseStep availableTestCaseStep = testCaseStepFacade.findOne((Long) testStepId);
                            availableSteps.addItem(availableTestCaseStep);
                            availableSteps.setItemCaption(availableTestCaseStep, availableTestCaseStep.getCaseStepName());

                            getAddButton(tabInd).setEnabled(true);
                        }
                    });
                }
            }
        }

        return removeButton;
    }

    private Button getUpButton(final int tabInd) {

        if (upButton == null) {
            synchronized (this) {
                if (upButton == null) {
                    upButton = new Button("Up");
                    upButton.setEnabled(false);

                    upButton.addClickListener(new Button.ClickListener() {
                        public void buttonClick(Button.ClickEvent event) {
                            Table stepsTable = getCurrentChildesTable(tabInd);
                            Long stepId = (Long) stepsTable.getValue();
                            BeanContainer<Long, TestCaseStep> beanContainer = getCurrentChildContainer(tabInd);
                            int position = beanContainer.indexOfId(stepId);

                            if (position > 0) {
                                testCaseFacade.moveStepUp(getSelectedParentId(), position);
                                BeanItem<TestCaseStep> beanItem = beanContainer.getItem(stepId);
                                beanContainer.removeItem(stepId);
                                beanContainer.addBeanAt(position - 1, beanItem.getBean());
                                stepsTable.refreshRowCache();

                                changeChildToolbarButtonsState(stepId, tabInd);
                            }
                        }
                    });
                }
            }
        }

        return upButton;
    }

    private Button getDownButton(final int tabInd) {
        if (downButton == null) {
            synchronized (this) {
                if (downButton == null) {
                    downButton = new Button("Down");
                    downButton.setEnabled(false);

                    downButton.addClickListener(new Button.ClickListener() {
                        public void buttonClick(Button.ClickEvent event) {
                            Table stepsTable = getCurrentChildesTable(tabInd);
                            Long stepId = (Long) stepsTable.getValue();
                            BeanContainer<Long, TestCaseStep> beanContainer = getCurrentChildContainer(tabInd);
                            int position = beanContainer.indexOfId(stepId);

                            if (position < beanContainer.size()) {
                                testCaseFacade.moveStepDown(getSelectedParentId(), position);
                                BeanItem<TestCaseStep> beanItem = beanContainer.getItem(stepId);
                                beanContainer.removeItem(stepId);
                                beanContainer.addBeanAt(position + 1, beanItem.getBean());
                                stepsTable.refreshRowCache();

                                changeChildToolbarButtonsState(stepId, tabInd);
                            }
                        }
                    });
                }
            }
        }

        return downButton;
    }

    private Button getAddButton(final int tabInd) {
        if (addButton == null) {
            synchronized (this) {
                if (addButton == null) {

                    addButton = new Button("Add:");
                    addButton.addClickListener(new Button.ClickListener() {
                        public void buttonClick(Button.ClickEvent event) {
                            TestCaseStep testCaseStep = (TestCaseStep) availableSteps.getValue();

                            if (testCaseStep == null) {
                                Notification.show("Step is not selected", "Please select step to add.",
                                        Notification.Type.ASSISTIVE_NOTIFICATION);
                            } else {

                                BeanContainer<Long, TestCaseStep> beanContainer = getCurrentChildContainer(tabInd);
                                beanContainer.addBean(testCaseStep);

                                getCurrentChildesTable(tabInd).refreshRowCache();

                                testCaseFacade.addTestCaseStep(getSelectedParentId(), testCaseStep.getId());
                                availableSteps.removeItem(testCaseStep);

                                if (availableSteps.size() == 0) {
                                    getAddButton(tabInd).setEnabled(false);
                                }
                            }
                        }
                    });
                }
            }
        }

        return addButton;
    }

    protected void onChildTableValueCHanged(Property property, int tabInd) {
        Table childTable = getCurrentChildesTable(tabInd);
        BeanContainer<Long, TestCaseStep> childContainer = getCurrentChildContainer(tabInd);

        if (childTable.getValue() == null || childContainer.size() == 0) {
            removeButton.setEnabled(false);
            upButton.setEnabled(false);
            downButton.setEnabled(false);
        } else {
            removeButton.setEnabled(true);
        }
    }

    @Override
    protected void onChildTableItemClick(ItemClickEvent event, int tabInd) {
        changeChildToolbarButtonsState(event.getItemId(), tabInd);
    }

    private void changeChildToolbarButtonsState(Object itemId, int tabInd) {
        Button upButton = getUpButton(tabInd);
        Button downButton = getDownButton(tabInd);

        BeanContainer<Long, TestCaseStep> childContainer = getCurrentChildContainer(tabInd);

        upButton.setEnabled(!childContainer.isFirstId(itemId));
        downButton.setEnabled(!childContainer.isLastId(itemId));
    }

    @Override
    protected String getChildIdName(int tabInd) {
        return GenericView.REQUEST_TEST_CASE_STEP_ID;
    }

    protected void onParentListItemSelected(Long parentId) {
        super.onParentListItemSelected(parentId);

        List<TestCaseStep> testCaseSteps =
                testCaseStepFacade.getTestCaseStepsForTestCase(parentId);
        availableSteps.removeAllItems();
        for (TestCaseStep testCaseStep : testCaseSteps) {
            if (testCaseStep != null) {
                availableSteps.addItem(testCaseStep);
                availableSteps.setItemCaption(testCaseStep, testCaseStep.getCaseStepName());
            }
        }

        addButton.setEnabled(testCaseSteps.size() > 0);
    }


    @Override
    protected String getParentIdName() {
        return GenericView.REQUEST_TEST_CASE_ID;
    }

    @Override
    protected List<Component> createEditorToolbarComponents() {
        Button runCaseButton = new Button("Run");

        runCaseButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Long testCaseId = getSelectedParentId();
                List<TestCaseStep> testCaseSteps = testCaseStepFacade.getTestCaseSteps(testCaseId);

                if (testCaseSteps != null && testCaseSteps.size() > 0) {
                    try {
                        Worker worker = applicationContext.getBean(Worker.class);
                        String result = worker.runScript(testCaseSteps);
                        testCaseFacade.setTestCaseResult(testCaseId, result);
                    } catch (ReflectiveOperationException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        List<Component> toolBarComponents = new ArrayList<>();
        toolBarComponents.add(runCaseButton);
        return toolBarComponents;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
