package com.dyggyty.groper.ui;

import com.dyggyty.groper.model.GenericIdEntity;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author mobius
 */
public abstract class ListView<PARENT extends GenericIdEntity> extends AuthenticatedView {

    protected static final String TITLE_NAME = "Name";
    protected static final String TITLE_ID = "#";
    protected static final String TITLE_CREATED = "Created";
    protected static final String TITLE_DESC = "Description";
    protected static final String TITLE_DETAILS = "Details";

    private Table parentsList = new Table();
    private TextField searchField = new TextField();

    private Button addNewButton = new Button("New");
    private Button removeParentButton = new Button("Delete");
    private Button saveParentButton = new Button("Save Changes");

    private FieldGroup editorFields = new FieldGroup();
    private TabSheet tabSheet = new TabSheet();

    private BeanContainer<Long, PARENT> parentsContainer = createParentDataSource();

    protected void initComponents(){
        initLayout();
        initParentList();
        initEditor();
        initSearch();
        initButtons();
    }

    protected void initData(){
        List<PARENT> parentsList = getParentsList();
        parentsContainer.removeAllItems();
        parentsContainer.addAll(parentsList);
    }

    private void initLayout() {

        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.setSizeFull();
        addComponent(splitPanel);
        setExpandRatio(splitPanel, 1);

        VerticalLayout leftLayout = new VerticalLayout();
        leftLayout.setSizeFull();

        parentsList.setSizeFull();

        List<Component> toolbarComponents = createListToolbarComponents();
        if (toolbarComponents != null && !toolbarComponents.isEmpty()) {
            HorizontalLayout toolbar = new HorizontalLayout();
            toolbar.setWidth(null);
            toolbar.setSpacing(true);

            toolbar.addComponents(toolbarComponents.toArray(new Component[toolbarComponents.size()]));

            leftLayout.addComponent(toolbar);
        }

        leftLayout.addComponent(parentsList);
        leftLayout.setExpandRatio(parentsList, 1);

        HorizontalLayout bottomLeftLayout = new HorizontalLayout();
        leftLayout.addComponent(bottomLeftLayout);
        bottomLeftLayout.addComponent(searchField);
        bottomLeftLayout.addComponent(addNewButton);

        bottomLeftLayout.setWidth(100, Unit.PERCENTAGE);
        searchField.setWidth(100, Unit.PERCENTAGE);
        bottomLeftLayout.setExpandRatio(searchField, 1);

        splitPanel.setFirstComponent(leftLayout);

        tabSheet.setVisible(false);
        tabSheet.setSizeFull();

        splitPanel.setFirstComponent(leftLayout);
        splitPanel.setSecondComponent(tabSheet);
    }

    private void initParentList() {

        parentsList.setContainerDataSource(parentsContainer);

        for (Map.Entry<String, String> parentTableHeaderEntry : getParentListHeaders().entrySet()) {
            parentsList.setColumnHeader(parentTableHeaderEntry.getKey(), parentTableHeaderEntry.getValue());
        }

        parentsList.setVisibleColumns(getVisibleColumns());
        parentsList.setSelectable(true);
        parentsList.setImmediate(true);

        parentsList.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {

                Object parentId = parentsList.getValue();

                if (parentId != null) {
                    editorFields.setItemDataSource(parentsList.getItem(parentId));
                    onParentListItemSelected((Long) parentId);
                }

                tabSheet.setVisible(parentId != null);
            }
        });
    }

    protected void initEditor() {
        HorizontalLayout toolbarLayout = new HorizontalLayout();
        toolbarLayout.setWidth(null);
        toolbarLayout.setHeight(null);
        toolbarLayout.setSpacing(true);

        toolbarLayout.addComponent(removeParentButton);
        toolbarLayout.addComponent(saveParentButton);

        List<Component> toolbarComponents = createEditorToolbarComponents();
        if (toolbarComponents != null && !toolbarComponents.isEmpty()) {
            toolbarLayout.addComponents(toolbarComponents.toArray(new Component[toolbarComponents.size()]));
        }

        FormLayout editorLayout = new FormLayout();

        //Initialize project information editor
        editorLayout.setMargin(true);
        editorLayout.setWidth(100, Unit.PERCENTAGE);
        editorLayout.addComponent(toolbarLayout);

        Map<String, AbstractField> editorFieldsMap = createEditorFields();
        for (Map.Entry<String, AbstractField> editorFieldEntry : editorFieldsMap.entrySet()) {
            AbstractField field = editorFieldEntry.getValue();
            editorLayout.addComponent(field);
            editorFields.bind(field, editorFieldEntry.getKey());
        }

        editorFields.setBuffered(false);

        tabSheet.addTab(editorLayout, TITLE_DETAILS);
    }

    private void initSearch() {

        searchField.setInputPrompt("Search");
        searchField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.LAZY);
        searchField.addTextChangeListener(new FieldEvents.TextChangeListener() {
            public void textChange(final FieldEvents.TextChangeEvent event) {

                parentsContainer.removeAllContainerFilters();
                parentsContainer.addContainerFilter(new EntityFilter(event.getText()));
            }
        });
    }

    private void initButtons() {
        addNewButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {

                parentsContainer.removeAllContainerFilters();

                PARENT parent = createEmptyParent();
                parent = saveParent(parent);

                BeanItem<PARENT> parentBeanItem = parentsContainer.addBeanAt(0, parent);
                parentsList.select(parentBeanItem);

                Serializable parentId = parent.getId();
                parentsList.setValue(parentId);

                editorFields.setItemDataSource(parentBeanItem);
                onParentListItemSelected((Long) parent.getId());
                tabSheet.setVisible(true);
            }
        });

        removeParentButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                Long parentId = getSelectedParentId();
                deleteParent(parentId);
                parentsList.removeItem(parentId);

                tabSheet.setVisible(false);
            }
        });


        saveParentButton.addClickListener(new Button.ClickListener() {
            @SuppressWarnings("unchecked")
            public void buttonClick(Button.ClickEvent event) {
                Object parentId = parentsList.getValue();
                PARENT parent = ((BeanItem<PARENT>) parentsList.getItem(parentId)).getBean();
                saveParent(parent);

                parentsList.refreshRowCache();
            }
        });
    }

    protected Long getSelectedParentId() {
        return (Long) parentsList.getValue();
    }

    @SuppressWarnings("unchecked")
    protected PARENT getSelectedParent() {
        Object parentId = parentsList.getValue();
        return ((BeanItem<PARENT>) parentsList.getItem(parentId)).getBean();
    }

    protected void onParentListItemSelected(Long parentId) {
    }

    protected TabSheet getTabSheet() {
        return tabSheet;
    }

    protected Table getParentsTable() {
        return parentsList;
    }

    /**
     * Creates empty parent object from template.
     *
     * @return parent entity object
     */
    protected abstract PARENT createEmptyParent();

    /**
     * Save parent object to the database.
     *
     * @param parent Object to save.
     * @return Saved parent instance.
     */
    protected abstract PARENT saveParent(PARENT parent);

    /**
     * Delete parent object from the database.
     *
     * @param parentId Database identifier of object to be deleted.
     */
    protected abstract void deleteParent(Long parentId);

    /**
     * Create data source for parent entities.
     *
     * @return New BeanContainer object
     */
    protected abstract BeanContainer<Long, PARENT> createParentDataSource();

    /**
     * Retrieve parents list for current view.
     *
     * @return List of parents.
     */
    protected abstract List<PARENT> getParentsList();

    /**
     * Retrurn ID of the field will be used as a filter.
     *
     * @return Field ID.
     */
    protected abstract String getFilteredFieldId();

    /**
     * Return parent list table headers.
     *
     * @return Map of headers FIELD_ID, FIELD_TITLE.
     */
    protected abstract Map<String, String> getParentListHeaders();

    /**
     * Return parent list visible columns.
     *
     * @return Array of visible columns.
     */
    protected abstract String[] getVisibleColumns();

    /**
     * Created fields for parent editor.
     *
     * @return Map of the editor fields.
     */
    protected abstract Map<String, AbstractField> createEditorFields();

    protected List<Component> createListToolbarComponents() {
        return null;
    }

    protected List<Component> createEditorToolbarComponents() {
        return null;
    }

    private class EntityFilter implements Container.Filter {
        private String needle;

        public EntityFilter(String needle) {
            this.needle = needle.toLowerCase();
        }

        public boolean passesFilter(Object itemId, Item item) {
            return item.getItemProperty(getFilteredFieldId()).getValue().toString().toLowerCase().contains(needle);
        }

        public boolean appliesToProperty(Object id) {
            return true;
        }
    }
}
