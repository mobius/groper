package com.dyggyty.groper.ui;

import com.dyggyty.groper.model.GenericEntity;
import com.dyggyty.groper.model.GenericIdEntity;
import com.dyggyty.groper.utils.ClassUtils;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author mobius
 */
public abstract class ChildAwareListView<PARENT extends GenericIdEntity, CHILD extends GenericEntity>
        extends ListView<PARENT> {

    private static final String CHILD_URL_FORMAT = "%s/%s,%s/%s,%s";

    private List<BeanContainer<Long, CHILD>> childesContainers = new ArrayList<>();
    private Table[] chidesTabs = new Table[getTabsCount()];

    public ChildAwareListView() {
        for (int tabInd = 0; tabInd < getTabsCount(); tabInd++) {
            childesContainers.add(createChildDataSource(tabInd));
        }
    }

    @SuppressWarnings("unchecked")
    protected void onParentListItemSelected(Long parentId) {
        for (int tabInd = 0; tabInd < getTabsCount(); tabInd++) {
            BeanContainer<Long, CHILD> childContainer = childesContainers.get(tabInd);
            childContainer.removeAllItems();
            childContainer.addAll(getChildren(tabInd, parentId));
        }
    }

    protected void initEditor() {
        super.initEditor();

        for (int tabInd = 0; tabInd < getTabsCount(); tabInd++) {
            VerticalLayout childLayout = createChildesTab(tabInd);
            getTabSheet().addTab(childLayout, getChildrenTabTitle(tabInd));
        }
    }

    private VerticalLayout createChildesTab(final int tabInd) {

        Table childesTab = new Table();
        chidesTabs[tabInd] = childesTab;
        childesTab.setSizeFull();

        childesTab.setContainerDataSource(childesContainers.get(tabInd));

        for (Map.Entry<String, String> childTableHeaderEntry : getChildListHeaders(tabInd).entrySet()) {
            childesTab.setColumnHeader(childTableHeaderEntry.getKey(), childTableHeaderEntry.getValue());
        }

        childesTab.setVisibleColumns(getChildVisibleColumns(tabInd));
        childesTab.setSelectable(true);
        childesTab.setImmediate(true);

        childesTab.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    Object parentId = getParentsTable().getValue();
                    Object childId = event.getItemId();

                    if (parentId != null && childId != null) {
                        String childUrl =
                                String.format(CHILD_URL_FORMAT, ClassUtils.getClassName(getChildrenClassView(tabInd)),
                                        getParentIdName(), parentId, getChildIdName(tabInd), childId);
                        getUI().getNavigator().navigateTo(childUrl);
                    }
                } else {
                    onChildTableItemClick(event, tabInd);
                }
            }
        });
        childesTab.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                onChildTableValueCHanged(valueChangeEvent.getProperty(), tabInd);
            }
        });

        Component toolBar;
        List<Component> childListToolbarItems = createChildListToolbarItems(tabInd);
        if (childListToolbarItems.size() > 1) {

            //In case we have several components in the tool bar
            HorizontalLayout toolBarLayout = new HorizontalLayout();
            toolBarLayout.setWidth(null);
            toolBarLayout.setHeight(null);
            toolBarLayout.setSpacing(true);
            toolBarLayout.addComponents(childListToolbarItems.toArray(new Component[childesContainers.size()]));
            toolBar = toolBarLayout;
        } else if (childListToolbarItems.size() == 1) {

            //If we have only one component then we don't need separate panel for it
            toolBar = childListToolbarItems.get(0);
        } else {
            toolBar = null;
        }

        VerticalLayout childesLayout = new VerticalLayout();
        childesLayout.setMargin(true);
        if (toolBar != null) {
            childesLayout.addComponent(toolBar);
        }
        childesLayout.addComponent(childesTab);
        childesLayout.setExpandRatio(childesTab, 1);
        childesLayout.setSizeFull();

        return childesLayout;
    }

    protected void onChildTableValueCHanged(Property property, int tabInd) {
    }

    protected void onChildTableItemClick(ItemClickEvent event, int tabInd) {
    }

    protected Table getCurrentChildesTable(int tabInd) {
        return chidesTabs[tabInd];
    }

    protected BeanContainer<Long, CHILD> getCurrentChildContainer(int tabInd) {
        return childesContainers.get(tabInd);
    }

    /**
     * Creates list of components for child toolbar.
     *
     * @param tabInd Tab index.
     * @return List of components for the toolbar.
     */
    protected List<Component> createChildListToolbarItems(final int tabInd) {
        List<Component> components = new ArrayList<>();

        //Buttons configuration
        Button addNewTestCaseButton = new Button("New");
        addNewTestCaseButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                getUI().getNavigator().navigateTo(ClassUtils.getClassName(getChildrenClassView(tabInd)) + "/" +
                        getParentIdName() + "," + getParentsTable().getValue());
            }
        });
        components.add(addNewTestCaseButton);

        return components;
    }

    protected abstract String getChildIdName(int tabInd);

    protected abstract String getParentIdName();

    /**
     * Return number of tabs.
     *
     * @return Tabs count.
     */
    protected abstract int getTabsCount();

    /**
     * Create data source for child entities.
     *
     * @param tabInd Tab index.
     * @return New BeanContainer object
     */
    protected abstract BeanContainer<Long, CHILD> createChildDataSource(int tabInd);

    /**
     * Return childes for current parent.
     *
     * @param tabInd   Tab index.
     * @param parentId Parent database identifier.
     * @return List of childes.
     */
    protected abstract Collection<CHILD> getChildren(int tabInd, Long parentId);

    /**
     * Return parent list table headers.
     *
     * @return Map of headers FIELD_ID, FIELD_TITLE.
     */
    protected abstract Map<String, String> getChildListHeaders(int tabInd);

    /**
     * Return parent list visible columns.
     *
     * @return Array of visible columns.
     */
    protected abstract String[] getChildVisibleColumns(int tabInd);

    /**
     * Return childes tab title.
     *
     * @param tabInd Tab index.
     * @return Childes tab title.
     */
    protected abstract String getChildrenTabTitle(int tabInd);

    /**
     * Return childes class view.
     *
     * @return Childes calss view.
     */
    protected abstract Class<? extends AuthenticatedView> getChildrenClassView(int tabInd);
}
