package com.dyggyty.groper.ui;

import com.dyggyty.groper.facade.UserProfileFacade;
import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.utils.ClassUtils;
import com.dyggyty.groper.utils.MessageException;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SignUpView extends HorizontalLayout implements View {

    @Autowired
    private UserProfileFacade userProfileFacade;

    private final TextField username;
    private final PasswordField password;
    private final TextField firstName;
    private final TextField lastName;
    private final TextField email;

    public SignUpView() {
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setSizeFull();
        setSpacing(true);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);
        verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        verticalLayout.setWidth(null);

        addComponent(verticalLayout);

        username = new TextField("Username");
        username.setRequired(true);

        password = new PasswordField("Password");
        password.setRequired(true);

        firstName = new TextField("First Name");
        lastName = new TextField("Last Name");
        email = new TextField("E-mail");

        verticalLayout.addComponent(username);
        verticalLayout.addComponent(password);
        verticalLayout.addComponent(firstName);
        verticalLayout.addComponent(lastName);
        verticalLayout.addComponent(email);

        verticalLayout.addComponent(createSignUpButton());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    private Button createSignUpButton() {
        return new Button("Sign up", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                UserProfile userProfile = new UserProfile();

                userProfile.setUserName(username.getValue());
                userProfile.setFirstName(firstName.getValue());
                userProfile.setLastName(lastName.getValue());
                userProfile.setEmail(email.getValue());

                boolean successFlag;
                try {
                    successFlag = userProfileFacade.signUp(userProfile, password.getValue());
                    if (!successFlag) {
                        Notification.show("Can't register user", Notification.Type.ERROR_MESSAGE);
                    }
                } catch (MessageException me) {
                    successFlag = false;
                    Notification.show(me.getLocalizedMessage(), Notification.Type.ERROR_MESSAGE);
                }

                if (successFlag) {
                    getUI().getNavigator().navigateTo("");
                }
            }
        });
    }
}
