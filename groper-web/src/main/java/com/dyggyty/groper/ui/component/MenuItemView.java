package com.dyggyty.groper.ui.component;

/**
 * @author mobius
 */
public interface MenuItemView {

    public String getHeading();

    /**
     * @return Menu order position.
     */
    public int getOrder();
}
