package com.dyggyty.groper.ui.editor;

import com.dyggyty.groper.worker.GroperScriptableObject;
import com.google.common.base.Strings;
import org.mozilla.javascript.BaseFunction;
import org.vaadin.aceeditor.Suggester;
import org.vaadin.aceeditor.Suggestion;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author mobius
 */
public class ScriptSuggester implements Suggester {

    private static final String FUNCTION_PREFIX = "jsFunction_";

    private Map<String, GroperScriptableObject> scriptableObjects = new HashMap<>();

    public ScriptSuggester(List<GroperScriptableObject> scriptableObjects) {
        for (GroperScriptableObject scriptableObject : scriptableObjects) {
            this.scriptableObjects.put(scriptableObject.getContextName(), scriptableObject);
        }
    }

    @Override
    public List<Suggestion> getSuggestions(String text, int cursor) {

        String currentLine = getCurrentLine(text, cursor);
        currentLine = normalizeLine(currentLine);
        LineItem lineItem = createLineItem(currentLine);

        LinkedList<Suggestion> suggs = new LinkedList<>();

        if (lineItem.getParent() == null) {
            for (Map.Entry<String, GroperScriptableObject> scriptableObjectEntry : scriptableObjects.entrySet()) {
                if (scriptableObjectEntry.getKey().startsWith(lineItem.getName())) {
                    String functionName = scriptableObjectEntry.getValue().getContextName();

                    if (scriptableObjectEntry.getValue() instanceof BaseFunction) {
                        functionName += "()";
                    }
                    //todo add description here
                    suggs.add(new Suggestion(functionName, "", functionName.substring(lineItem.getName().length())));
                }
            }
        } else {
            List<Method> methods = getMethods(lineItem);

            for (Method method : methods) {
                String methodName = method.getName().substring(FUNCTION_PREFIX.length());

                StringBuilder parameters = new StringBuilder();
                for (Class parameterType : method.getParameterTypes()) {
                    if (parameters.length() > 0) {
                        parameters.append(", ");
                    }
                    parameters.append(parameterType.getSimpleName());
                }

                //todo add description here
                suggs.add(new Suggestion(methodName + "(" + parameters + ")", "",
                        methodName.substring(lineItem.getName().length()) + "()"));
            }
        }

        return suggs;
    }

    /**
     * Retrieve list of JS methods of the current line item.<br/>
     * Current implementation will not work with reloaded functions.
     *
     * @param lineItem Top level LineItem object.
     * @return List of Methods.
     */
    public List<Method> getMethods(LineItem lineItem) {

        List<Method> result = new ArrayList<>();

        if (lineItem.getParent() == null) {
            GroperScriptableObject groperScriptableObject = scriptableObjects.get(lineItem.getName());

            if (groperScriptableObject != null) {
                Method[] methods = groperScriptableObject.getClass().getMethods();

                for (Method method : methods) {
                    if (method.getName().startsWith(FUNCTION_PREFIX)) {
                        result.add(method);
                    }
                }
            }
        } else {
            List<Method> parentMethods = getMethods(lineItem.getParent());
            for (Method method : parentMethods) {

                if (lineItem.getChild() == null) {
                    if (method.getName().startsWith(FUNCTION_PREFIX + lineItem.getName())) {
                        result.add(method);
                    }
                } else {
                    if (method.getName().equals(FUNCTION_PREFIX + lineItem.getName())) {
                        result.add(method);
                        break;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public String applySuggestion(Suggestion sugg, String text, int cursor) {
        // sugg is one of the objects returned by getSuggestions -> it's a MySuggestion.
        String ins = sugg.getSuggestionText();
        return text.substring(0, cursor) + ins + text.substring(cursor);
    }

    private static LineItem createLineItem(String line) {
        String[] items = line.split("\\.");

        LineItem result = null;

        for (String item : items) {
            LineItem child = new LineItem(item);

            if (result != null) {
                child.setParent(result);
                result.setChild(child);
            }

            result = child;
        }

        if (line.endsWith(".")) {
            LineItem child = new LineItem("");

            if (result != null) {
                child.setParent(result);
                result.setChild(child);
            }

            result = child;
        }

        return result;
    }

    private static String normalizeLine(String text) {
        return text.trim().replaceAll("\n", "").replaceAll(" ", "");
    }

    private static String getCurrentLine(String text, int cursor) {
        if (Strings.isNullOrEmpty(text) || cursor > text.length()) {
            return "";
        }

        String textToWork = text.substring(0, cursor);
        int terminationLineInd = Math.max(textToWork.lastIndexOf(";"), textToWork.lastIndexOf("{"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("}"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf(")"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("("));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("="));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf(">"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("<"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("+"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("-"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("/"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("*"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("%"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("|"));
        terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("&"));

        if (textToWork.lastIndexOf("else") > 0) {
            terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("else") + 3);
        }

        if (textToWork.lastIndexOf("return") > 0) {
            terminationLineInd = Math.max(terminationLineInd, textToWork.lastIndexOf("return") + 5);
        }

        terminationLineInd++;

        if (terminationLineInd > 0) {
            return textToWork.substring(terminationLineInd);
        } else {
            return textToWork;
        }
    }

    private static class LineItem {

        private LineItem parent;
        private LineItem child;
        private String name;

        private LineItem(String name) {
            this.name = name;
        }

        public LineItem getParent() {
            return parent;
        }

        public void setParent(LineItem parent) {
            this.parent = parent;
        }

        public LineItem getChild() {
            return child;
        }

        public void setChild(LineItem child) {
            this.child = child;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
