package com.dyggyty.groper.ui;

import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.ui.component.Breadcrumbs;
import com.dyggyty.groper.ui.component.MenuFactory;
import com.dyggyty.groper.ui.component.MenuItemView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * @author mobius
 */
public abstract class AuthenticatedView extends VerticalLayout implements View {

    @Autowired
    private MenuFactory menuFactory;

    private final Map<String, String> parameters = new HashMap<>();

    protected String getMessage(String key) {
        return ((GroperUI) getUI()).getMessage(key);
    }

    protected String getParameter(String paramKey) {
        return parameters.get(paramKey);
    }

    protected Long getLongParameter(String paramKey) {
        return Long.valueOf(parameters.get(paramKey));
    }

    protected boolean hasParameter(String paramKey){
        return parameters.containsKey(paramKey);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if (getCurrentUserProfile() == null) {
            getUI().getNavigator().navigateTo("");
            return;
        }

        parameters.clear();

        String[] parametersPairs = event.getParameters().split("/");
        for (String parameterPair : parametersPairs) {
            if (!StringUtils.isEmpty(parameterPair)) {
                String[] parameter = parameterPair.split(",");
                parameters.put(parameter[0], parameter.length > 1 ? parameter[1] : Boolean.TRUE.toString());
            }
        }

        boolean initialized = getComponentCount() != 0;

        VaadinSession session = getUI().getSession();
        Breadcrumbs breadcrumbs = session.getAttribute(Breadcrumbs.class);
        if (breadcrumbs == null) {
            breadcrumbs = new Breadcrumbs();
            session.setAttribute(Breadcrumbs.class, breadcrumbs);
        }

        breadcrumbs.navigate(event.getViewName(), getHeading(), parameters, this instanceof MenuItemView);

        if (!initialized) {
            setSizeFull();
            setSpacing(true);

            VerticalLayout headerLayout = new VerticalLayout();
            headerLayout.setSpacing(true);
            headerLayout.addComponent(menuFactory.getMenu());
            headerLayout.addComponent(createBredCrumbs());
            headerLayout.setWidth(100, Unit.PERCENTAGE);

            addComponent(headerLayout);
            initComponents();
        }

        initData();
    }

    protected abstract void initComponents();

    protected abstract void initData();

    protected UserProfile getCurrentUserProfile() {
        return getUI().getSession().getAttribute(UserProfile.class);
    }

    private HorizontalLayout createBredCrumbs() {
        HorizontalLayout bredCrumbsLayout = new HorizontalLayout();
        bredCrumbsLayout.setSpacing(true);

        final Breadcrumbs breadcrumbs = getUI().getSession().getAttribute(Breadcrumbs.class);
        List<Breadcrumbs.BreadcrumbTrail> breadcrumbTrails = breadcrumbs.getBreadcrumbTrails();

        if (breadcrumbTrails.size() > 1) {
            Button backButton = new Button("Back", new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    breadcrumbs.stepBack(getUI().getNavigator());
                }
            });

            bredCrumbsLayout.addComponent(backButton);
        }

        for (int i = 0; i < breadcrumbTrails.size(); i++) {
            Breadcrumbs.BreadcrumbTrail breadcrumbTrail = breadcrumbTrails.get(i);

            if (i < breadcrumbTrails.size() - 1) {
                final String viewName = breadcrumbTrail.getViewName();
                Button button = new Button(breadcrumbTrail.getTitle(), new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        breadcrumbs.stepTo(viewName, getUI().getNavigator());
                    }
                });
                button.setStyleName(Reindeer.BUTTON_LINK);

                bredCrumbsLayout.addComponent(button);
                bredCrumbsLayout.addComponent(new Label(">"));
            } else {
                bredCrumbsLayout.addComponent(new Label(breadcrumbTrail.getTitle()));
            }
        }

        return bredCrumbsLayout;
    }

    protected abstract String getHeading();
}
