package com.dyggyty.groper.ui;

import com.dyggyty.groper.ui.component.MenuItemView;
import com.vaadin.navigator.View;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HomeView extends AuthenticatedView implements View, MenuItemView {

    private static final String VIEW_NAME = "Home";

    @Override
    protected void initComponents() {
        //todo implement this
    }

    @Override
    protected void initData() {
        //todo implement this
    }

    @Override
    public String getHeading() {
        return VIEW_NAME;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
