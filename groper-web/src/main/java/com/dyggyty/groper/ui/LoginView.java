package com.dyggyty.groper.ui;

import com.dyggyty.groper.facade.UserProfileFacade;
import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.utils.ClassUtils;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author mobius
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LoginView extends GenericView implements View {

    @Autowired
    private UserProfileFacade userProfileFacade;

    private TextField username;
    private PasswordField password;

    @Override
    public void enter(ViewChangeEvent event) {
        super.enter(event);

        UserProfile userProfile = getUI().getSession().getAttribute(UserProfile.class);

        if (userProfile != null) {
            getUI().getNavigator().navigateTo(ClassUtils.getClassName(HomeView.class));
            return;
        }

        if (getComponentCount() == 0) {
            setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            setSizeFull();
            setSpacing(true);

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.setSpacing(true);
            verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            verticalLayout.setWidth(null);

            addComponent(verticalLayout);

            Label label = new Label("Enter your information below to log in.");
            label.setWidth(null);
            username = new TextField("Username");
            username.setId("userName");

            password = new PasswordField("Password");
            password.setId("userPassword");

            verticalLayout.addComponent(label);
            verticalLayout.addComponent(username);
            verticalLayout.addComponent(password);

            HorizontalLayout buttonsLayout = new HorizontalLayout();
            buttonsLayout.setSpacing(false);
            buttonsLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            buttonsLayout.setSizeFull();

            buttonsLayout.addComponent(createLoginButton());
            buttonsLayout.addComponent(createRegisterButton());

            verticalLayout.addComponent(buttonsLayout);
        }
    }

    private Button createRegisterButton() {
        Button button = new Button(getMessage(LABEL_SIGN_UP), new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent clickEvent) {
                getUI().getNavigator().navigateTo(ClassUtils.getClassName(SignUpView.class));
            }
        });
        button.setStyleName(Reindeer.BUTTON_LINK);
        return button;
    }

    private Button createLoginButton() {
        Button btn = new Button("Log In", new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                UserProfile userProfile = userProfileFacade.authenticateUser(username.getValue(), password.getValue());
                if (userProfile == null) {
                    Notification.show("Can't find user with specified user name and password combination",
                            Notification.Type.ERROR_MESSAGE);
                } else {
                    getUI().getSession().setAttribute(UserProfile.class, userProfile);
                    getUI().getNavigator().navigateTo(ClassUtils.getClassName(HomeView.class));
                }
            }
        });

        btn.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btn.addStyleName("primary");
        btn.setId("loginButton");

        return btn;
    }
}
