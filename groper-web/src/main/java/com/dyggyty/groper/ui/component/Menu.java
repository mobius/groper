package com.dyggyty.groper.ui.component;

import com.dyggyty.groper.model.UserProfile;
import com.dyggyty.groper.utils.ClassUtils;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Menu extends CustomComponent {

    Menu(ApplicationContext applicationContext) {
        HorizontalLayout layout = new HorizontalLayout();

        Map<String, MenuItemView> menuItems =
                BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, MenuItemView.class);

        List<MenuItemView> menuItemViews = new ArrayList<>(menuItems.values());

        Collections.sort(menuItemViews, new Comparator<MenuItemView>() {
            @Override
            public int compare(MenuItemView o1, MenuItemView o2) {
                return Integer.compare(o1.getOrder(), o2.getOrder());
            }
        });

        for (MenuItemView menuItem : menuItemViews) {
            layout.addComponent(generateMenuItem(menuItem));
        }

        layout.addComponent(logoutButton());
        layout.setSizeUndefined();
        layout.setSpacing(true);
        setSizeUndefined();
        setCompositionRoot(layout);
    }

    private Button generateMenuItem(final MenuItemView menuItem) {
        Button menuItemButton = new Button(menuItem.getHeading(), new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                getUI().getNavigator().navigateTo(ClassUtils.getClassName(menuItem.getClass()));
            }
        });
        menuItemButton.setId(menuItem.getHeading().replaceAll("\\W", ""));

        return menuItemButton;
    }

    private Button logoutButton() {
        return new Button("Logout", new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                getUI().getSession().setAttribute(UserProfile.class, null);
                getUI().getNavigator().navigateTo("");
            }
        });
    }
}
