package com.dyggyty.groper.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author vitaly.rudenya
 */
@Configuration
@EnableJpaRepositories("com.dyggyty.groper.dao")
@EnableTransactionManagement
@PropertySource("classpath:META-INF/spring/persistence.properties")
@Lazy
public class JpaConfig {
    private static final String[] JPA_PROPERTIES = new String[]{
            "hibernate.dialect",
            "hibernate.format_sql",
            "hibernate.ejb.naming_strategy",
            "hibernate.show_sql",
            "hibernate.hbm2ddl.auto",
            "hibernate.cache.use_second_level_cache",
            "hibernate.cache.use_query_cache",
            "javax.persistence.sharedCache.mode"};

    @Resource
    private Environment environment;

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty("entitymanager.packages.to.scan"));
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setJpaProperties(buildJpaProperties());

        return entityManagerFactoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
        return txManager;
    }

    @Bean
    public HibernateExceptionTranslator exceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    private Properties buildJpaProperties() {
        Properties jpaProperties = new Properties();
        for (String propertyName : JPA_PROPERTIES) {
            jpaProperties.put(propertyName, environment.getRequiredProperty(propertyName));
        }
        return jpaProperties;
    }
}
