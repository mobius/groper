package com.dyggyty.groper.config;

import com.vaadin.server.DefaultUIProvider;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringUIProvider extends DefaultUIProvider {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public UI createInstance(UICreateEvent event) {
        WebApplicationContext context = init();
        if (context == null) {
            throw new IllegalStateException("Couldn't load spring context.");
        }
        return context.getBean(event.getUIClass());
    }

    private WebApplicationContext init() {
        VaadinServlet vaadinServlet = VaadinServlet.getCurrent();
        if (vaadinServlet == null) {
            logger.error("Couldn't get current instance of VaadinServlet");
            return null;
        }
        return WebApplicationContextUtils.getRequiredWebApplicationContext(vaadinServlet.getServletContext());
    }
}
