package com.dyggyty.groper.config;

import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinServlet;
import javax.servlet.ServletException;
import org.hibernate.service.spi.ServiceException;

/**
 * @author mobius
 */
public class GroperVaadinServlet extends VaadinServlet {

    private static final String UI_PROVIDER = "UIProvider";
    private static volatile UIProvider uiProvider;

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();

        final String providerClassName = getServletConfig().getInitParameter(UI_PROVIDER);

        getService().addSessionInitListener(
                new SessionInitListener() {
                    @Override
                    public void sessionInit(SessionInitEvent event) throws ServiceException {
                        boolean configured = false;
                        for (UIProvider uiProvider : event.getSession().getUIProviders()) {
                            if (uiProvider.getClass().getName().equals(providerClassName)) {
                                configured = true;
                                break;
                            }
                        }

                        if (!configured) {
                            try {

                                event.getSession().addUIProvider(getUiProvider(providerClassName));
                            } catch (ReflectiveOperationException e) {
                                throw new ServiceException(e.getLocalizedMessage());
                            }
                        }
                    }
                }
        );

    }

    private UIProvider getUiProvider(String className) throws ReflectiveOperationException {
        if (uiProvider == null) {
            synchronized (this) {
                if (uiProvider == null) {
                    uiProvider = (UIProvider) Class.forName(className).newInstance();
                }
            }
        }

        return uiProvider;
    }
}
