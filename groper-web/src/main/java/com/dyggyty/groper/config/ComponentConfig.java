package com.dyggyty.groper.config;

import com.dyggyty.groper.worker.config.WorkerConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {"com.dyggyty.groper.ui", "com.dyggyty.groper.facade", "com.dyggyty.groper.bd", "com.dyggyty.groper.dao"},
        excludeFilters = {@ComponentScan.Filter(Configuration.class)})
@Import(WorkerConfig.class)
public class ComponentConfig {
}
