package com.dyggyty.groper.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * @author vitaly.rudenya
 */
public class JDBCDriverListener implements ServletContextListener {

	private final Log LOG = LogFactory.getLog(this.getClass());

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		//no implementation necessary
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
				LOG.info(String.format("deregistering jdbc driver: %s", driver));
			} catch (SQLException e) {
				LOG.error(String.format("Error deregistering driver %s", driver), e);
			}
		}
	}
}
