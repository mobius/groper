package com.dyggyty.groper.utils;

/**
 * @author mobius
 */
public class ClassUtils {

    private ClassUtils() {
    }

    public static String getClassName(Class classType) {
        String name = classType.getSimpleName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}
