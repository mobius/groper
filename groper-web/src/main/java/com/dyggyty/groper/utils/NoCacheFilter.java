package com.dyggyty.groper.utils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author vitaly.rudenya
 */
public class NoCacheFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//nothing here
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		HttpServletResponse httpRes = (HttpServletResponse) response;

		httpRes.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		httpRes.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		httpRes.setDateHeader("Expires", 0); // Proxies.

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		//nothing here
	}
}
