package com.dyggyty.groper.dao;

import com.dyggyty.groper.model.TestCaseStep;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author mobius
 */
@Repository
public interface TestCaseStepRepository extends CrudRepository<TestCaseStep, Long> {

    public static final String GET_BY_TEST_CASE_ID = "SELECT tc.caseSteps FROM TestCase tc " +
            "WHERE tc.id = :testCaseId";

    public static final String GET_FOR_TEST_CASE_ID = "SELECT testStep FROM TestCase projectCase\n" +
            "INNER JOIN projectCase.project project\n"+
            "LEFT JOIN project.testCaseSteps testStep\n"+
            "WHERE projectCase.id = :testCaseId AND NOT EXISTS (" +
            "SELECT caseStep.id FROM TestCase tc\n" +
            "LEFT JOIN tc.caseSteps caseStep\n"+
            "WHERE tc.id = :testCaseId AND caseStep.id = testStep.id" +
            ")";

    public static final String GET_BY_PROJECT_ID = "SELECT tcs FROM TestCaseStep tcs " +
            "LEFT JOIN tcs.project project WHERE project.id = :projectId";

    @Query(GET_BY_TEST_CASE_ID)
    public List<TestCaseStep> getTestCaseSteps(@Param("testCaseId") Long testCaseId);

    @Query(GET_BY_PROJECT_ID)
    public List<TestCaseStep> getTestCaseStepsForProject(@Param("projectId") Long projectId);

    @Query(GET_FOR_TEST_CASE_ID)
    public List<TestCaseStep> getTestCaseStepsForTestCase(@Param("testCaseId") Long testCaseId);
}
