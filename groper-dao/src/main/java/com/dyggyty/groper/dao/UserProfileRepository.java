package com.dyggyty.groper.dao;

import com.dyggyty.groper.model.EntityState;
import com.dyggyty.groper.model.UserProfile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * User: mobius
 * Date: 05.06.12
 * Time: 15:54
 */
@Repository
public interface UserProfileRepository extends CrudRepository<UserProfile, String> {

    public static final String GET_BY_USER_NAME_PASSWORD = "SELECT u FROM UserProfile u WHERE u.userName = :userName " +
            "AND u.password = :password AND u.entityState = :entityState";

    public static final String GET_BY_SESSION = "SELECT u FROM UserProfile u WHERE u.userSession = :userSession";

    public static final String GET_USER_SALT = "SELECT u.passwordSalt FROM UserProfile u WHERE u.userName = :userName";

    /**
     * Retrieve user by user name and encrypted password.
     *
     * @param userName User login name.
     * @param password Encrypted password.
     * @return User entity.
     */
    @Query(GET_BY_USER_NAME_PASSWORD)
    public UserProfile getUserByNameAndPassword(@Param("userName") String userName, @Param("password") String password,
                                                @Param("entityState") EntityState entityState);

    /**
     * Retrieve user by login name.
     *
     * @param userSession User session value.
     * @return Existing user object.
     */
    @Query(GET_BY_SESSION)
    public UserProfile getUserBySession(@Param("userSession") String userSession);

    /**
     * Retrieve user password salt
     *
     * @param userName User login name
     * @return Salt
     */
    @Query(GET_USER_SALT)
    public String getUserSalt(@Param("userName") String userName);
}
