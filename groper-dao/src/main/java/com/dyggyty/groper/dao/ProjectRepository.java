package com.dyggyty.groper.dao;

import com.dyggyty.groper.model.Project;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author mobius
 */
@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    public static final String GET_BY_USER_NAME = "SELECT pr FROM Project pr " +
            "INNER JOIN pr.owner ow " +
            "WHERE ow.userName = :userName";

    public static final String GET_BY_CASE_ID = "SELECT pr FROM Project pr " +
            "INNER JOIN pr.testCases tc " +
            "WHERE tc.id = :caseId";

    @Query(GET_BY_USER_NAME)
    public List<Project> getUserProjects(@Param("userName") String userName);

    @Query(GET_BY_CASE_ID)
    public Project findByCaseId(@Param("caseId") Long caseId);
}
