package com.dyggyty.groper.dao;

import com.dyggyty.groper.model.Project;
import com.dyggyty.groper.model.TestCase;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author mobius
 */
@Repository
public interface TestCaseRepository extends CrudRepository<TestCase, Long> {

    public static final String GET_BY_PROJECT = "SELECT tc FROM TestCase tc WHERE tc.project = :project";
    public static final String GET_BY_PROJECT_ID = "SELECT tc FROM TestCase tc "+
            "LEFT JOIN tc.project project WHERE project.id = :projectId";
    public static final String SET_TEST_CASE_RESULT = "UPDATE TestCase tc " +
            "SET tc.lastResult = :testCaseResult WHERE tc.id = :testCaseId";

    @Query(GET_BY_PROJECT)
    public List<TestCase> getTestCases(@Param("project") Project project);

    @Query(GET_BY_PROJECT_ID)
    public List<TestCase> getTestCases(@Param("projectId") Long projectId);

    @Modifying
    @Query(SET_TEST_CASE_RESULT)
    public void setTestCaseResult(@Param("testCaseId") Long testCaseId,
                                  @Param("testCaseResult") String testCaseResult);
}
