package com.dyggyty.groper.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author mobius
 */
@Entity
@Table(name = "test_case_step")
public class TestCaseStep extends GenericIdEntity<Long> {

    @Column(name = "step_name", nullable = false)
    private String caseStepName = "";

    @Column(name = "step_desc", nullable = false, length = 2048)
    private String description = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(name = "script", nullable = false, length = 65536)
    private String script = "";

    public String getCaseStepName() {
        return caseStepName;
    }

    public void setCaseStepName(String caseStepName) {
        this.caseStepName = caseStepName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
