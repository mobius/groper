package com.dyggyty.groper.model;

/**
 * @author mobius
 */
public enum EntityState {
    NEW, VERIFIED, ACTIVATE, SUSPENDED, TERMINATED
}
