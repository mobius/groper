package com.dyggyty.groper.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class GenericEntity implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date created = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date updated = new Date();

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "GenericEntity{" +
                "created=" + created +
                ", updated=" + updated +
                '}';
    }
}
