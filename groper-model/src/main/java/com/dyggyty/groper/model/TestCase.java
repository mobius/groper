package com.dyggyty.groper.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import java.util.List;

/**
 * @author mobius
 */
@Entity
@Table(name = "test_case")
public class TestCase extends GenericIdEntity<Long> {

    @Column(name = "case_name", nullable = false)
    private String caseName;

    @Column(name = "case_desc", nullable = false, length = 2048)
    private String caseDescription = "";

    @Column(name = "last_result", nullable = false, length = 2048)
    private String lastResult = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "test_case_steps")
    @OrderColumn(name="idx")
    private List<TestCaseStep> caseSteps;

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getLastResult() {
        return lastResult;
    }

    public void setLastResult(String lastResult) {
        this.lastResult = lastResult;
    }

    public List<TestCaseStep> getCaseSteps() {
        return caseSteps;
    }

    public void setCaseSteps(List<TestCaseStep> caseSteps) {
        this.caseSteps = caseSteps;
    }
}
