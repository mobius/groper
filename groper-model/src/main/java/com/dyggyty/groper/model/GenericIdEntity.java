package com.dyggyty.groper.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * @author vitaly.rudenya
 */
@MappedSuperclass
public class GenericIdEntity<ID extends Serializable> extends GenericEntity {

    @Id
    @GeneratedValue
    @Column
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GenericIdEntity that = (GenericIdEntity) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "GenericIdEntity{" +
                "id=" + id +
                "} " + super.toString();
    }
}
