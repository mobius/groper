package com.dyggyty.groper.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

/**
 * @author mobius
 */
@Entity
@Table(name = "project")
public class Project extends GenericIdEntity<Long> {

    @Column(name="project_name", nullable = false)
    private String projectName;

    @Column(name="project_desc", nullable = false, length = 2048)
    private String projectDescription = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="owner_profile", nullable=false)
    private UserProfile owner;

    @OneToMany(mappedBy ="project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<TestCase> testCases;

    @OneToMany(mappedBy ="project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TestCaseStep> testCaseSteps;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public UserProfile getOwner() {
        return owner;
    }

    public void setOwner(UserProfile owner) {
        this.owner = owner;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

    public Set<TestCaseStep> getTestCaseSteps() {
        return testCaseSteps;
    }

    public void setTestCaseSteps(Set<TestCaseStep> testCaseSteps) {
        this.testCaseSteps = testCaseSteps;
    }
}
